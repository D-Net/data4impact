--------------------------------------------------
--- A view giving the list of mat views that depend
--- on each mat view, view or table
--------------------------------------------------

--DROP VIEW IF EXISTS mat_view_refresh_order;
--DROP VIEW IF EXISTS mat_view_dependencies;

CREATE TEMPORARY VIEW mat_view_dependencies AS
        WITH RECURSIVE s(start_schemaname,start_relname,start_relkind, schemaname,mvname,relkind,mvoid,depth) AS (
                -- List of tables and views that mat views depend on
                SELECT
                        n.nspname AS start_schemaname,
                        c.relname AS start_relname,
                        c.relkind AS start_relkind,
                        n2.nspname AS schemaname,
                        c2.relname AS mvname,
                        c2.relkind,
                        c2.oid AS mvoid,
                        0 AS depth
                FROM
                        pg_class c
                        JOIN pg_namespace n ON c.relnamespace=n.oid AND c.relkind IN ('r','m','v','t','f')
                        JOIN pg_depend d ON c.oid=d.refobjid
                        JOIN pg_rewrite r ON d.objid=r.oid
                        JOIN pg_class c2 ON r.ev_class=c2.oid AND c2.relkind='m'
                        JOIN pg_namespace n2 ON n2.oid=c2.relnamespace

                UNION

                -- Recursively find all mat views depending on previous level
                SELECT
                        s.start_schemaname,
                        s.start_relname,
                        s.start_relkind,
                        n.nspname AS schemaname,
                        c.relname AS mvname,
                        c.relkind,
                        c.oid AS mvoid,
                        depth+1 AS depth
                FROM
                        s
                        JOIN pg_depend d ON s.mvoid=d.refobjid
                        JOIN pg_rewrite r ON d.objid=r.oid
                        JOIN pg_class c ON r.ev_class=c.oid AND (c.relkind IN ('m','v'))
                        JOIN pg_namespace n ON n.oid=c.relnamespace
                        WHERE s.mvoid <> c.oid -- exclude the current MV which always depends on itself
        )
        SELECT * FROM s;

--------------------------------------------------
--- A view that returns the list of mat views in the
--- order they should be refreshed.
--------------------------------------------------

CREATE TEMPORARY VIEW mat_view_refresh_order AS
        WITH b AS (
                -- Select the highest depth of each mat view name
                SELECT DISTINCT ON (schemaname,mvname) schemaname, mvname, depth
                FROM mat_view_dependencies
                WHERE relkind='m'
                ORDER BY schemaname, mvname, depth DESC
        )
        -- Reorder appropriately
        SELECT schemaname, mvname, depth AS refresh_order
        FROM b
        ORDER BY depth, schemaname, mvname;


-- Prepare the script
SELECT string_agg(
       'REFRESH MATERIALIZED VIEW "' || schemaname || '"."' || mvname || '";',
       E'\n' ORDER BY refresh_order) AS script
FROM mat_view_refresh_order \gset

-- Visualize the script
\echo :script

-- Execute the script
:script
