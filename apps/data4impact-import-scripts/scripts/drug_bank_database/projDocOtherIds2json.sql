COPY (SELECT row_to_json(t) FROM (
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	pmcid                            AS "docId",
	 	'pmcid'                          AS "docIdType"
	FROM data
	WHERE pmcid IS NOT NULL AND pmcid != '' AND pmcid not ilike 'none'
	
	UNION ALL
	
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	pmid                             AS "docId",
	 	'pmid'                           AS "docIdType"
	FROM data
	WHERE pmid IS NOT NULL AND pmid != '' AND pmid not ilike 'none'
	
	UNION ALL
	
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	doi                              AS "docId",
	 	'doi'                            AS "docIdType"
	FROM data
	WHERE doi IS NOT NULL AND doi != '' AND doi not ilike 'none'
	
	UNION ALL
	
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	d_b_id                           AS "docId",
	 	'drug_bank_id'                   AS "docIdType"
	FROM data
	WHERE d_b_id IS NOT NULL AND d_b_id != '' AND d_b_id not ilike 'none'
	
) t) TO STDOUT;
