COPY (SELECT row_to_json(t) FROM (
	SELECT
	 	pmcid   AS "id",
	 	'pmcid' AS "type"
	FROM data
	WHERE pmcid IS NOT NULL AND pmcid != '' AND pmcid not ilike 'none'
	
	UNION ALL
	
	SELECT
	 	pmid   AS "id",
	 	'pmid' AS "type"
	FROM data
	WHERE pmid IS NOT NULL AND pmid != '' AND pmid not ilike 'none'
	
	UNION ALL
	
	SELECT
	 	doi   AS "id",
	 	'doi' AS "type"
	FROM data
	WHERE doi IS NOT NULL AND doi != '' AND doi not ilike 'none'
	
	UNION ALL
	
	SELECT
	 	d_b_id         AS "id",
	 	'drug_bank_id' AS "type"
	FROM data
	WHERE d_b_id IS NOT NULL AND d_b_id != '' AND d_b_id not ilike 'none'

) t) TO STDOUT;