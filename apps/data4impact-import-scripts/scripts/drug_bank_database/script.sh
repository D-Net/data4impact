#!/bin/bash

excelFile="../../orig/drug_bank_database/Publication_citations_in_Drug_Bank_database.xlsx"



workdir=/tmp/drugbank
rm -rf "$workdir" && mkdir "$workdir"


echo
echo "Links from drugbank db Import:"


#--------------------------------
echo " - Generating csv file"
csv="$workdir/drugbank.csv"
xlsx2csv -c UTF-8 "$excelFile" > $csv

#--------------------------------
echo " - Recreating the drugbank database"
dbname=drugbank

dropdb $dbname --if-exists;
createdb $dbname;
psql $dbname -f schema.sql

if [[ -f "$csv" ]]; then
	echo " - Importing data: $csv"
	psql $dbname -c "COPY data(id,d_b_id,doi,pmcid,pmid,drug_substance,funder,section_of_drug_bank_entry_where_citation_occured) FROM '$csv' CSV HEADER;"
else
   	echo " - Invalid file: $csv"
fi


echo " - Fix funder names"

psql $dbname -c "UPDATE data SET funder='EC' WHERE funder = 'European Research Council'"
psql $dbname -c "UPDATE data SET funder='Austrian Science Fund FWF' WHERE funder = 'FWF'"
psql $dbname -c "UPDATE data SET funder='Swiss National Science Foundation SNSF' WHERE funder = 'Swiss National Science Foundation'"



#--------------------------------
echo " - Generating json files"
rm  -f ../../jsonfiles/drug_bank_database/*.json
psql $dbname -f projects2json.sql        | sed 's/\\\\/\\/g' > ../../jsonfiles/drug_bank_database/project.json
psql $dbname -f docOtherId2json.sql      | sed 's/\\\\/\\/g' > ../../jsonfiles/drug_bank_database/docotherid.json
psql $dbname -f projDocOtherIds2json.sql | sed 's/\\\\/\\/g' > ../../jsonfiles/drug_bank_database/projectdocotherid.json

echo "Done."
echo
