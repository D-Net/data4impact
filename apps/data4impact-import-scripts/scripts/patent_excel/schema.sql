CREATE TABLE data(
	pat_id              text,
	type_ip             text,
	appnum              text,
	appnt               text,
	title               text,
	pat_url             text,
	pat_ref             text,
	pat_auth            text,
	pat_num             text,
	pat_kind            text,
	note                text,
	appln_id            text,
	appln_title_patstat text,
	priority_year       text,
	var15               text,
	projectid           text
);



CREATE MATERIALIZED VIEW document AS SELECT
	'50|patents_____::'||MD5(lower(trim(appln_id))) AS "id",
	title                                           AS "title",
	lower(regexp_replace(type_ip,'s$',''))          AS "type",
	priority_year                                   AS "pubYear",
	'patent repo'::text                             AS "repository"
FROM data
WHERE appln_id IS NOT NULL AND trim(appln_id) != '';

CREATE MATERIALIZED VIEW doc_other_identifier AS SELECT
	'50|patents_____::'||MD5(lower(trim(appln_id))) AS "docId",
	trim(appln_id)                                  AS "id",
	'patent'::text                                       AS "type"
FROM data
WHERE appln_id IS NOT NULL AND trim(appln_id) != '';

CREATE MATERIALIZED VIEW doc_project AS SELECT
	'50|patents_____::'||MD5(lower(trim(appln_id)))  AS "docId",
	'40|corda_______::'||MD5(lower(trim(projectid))) AS "projectId"
FROM data
WHERE appln_id IS NOT NULL AND trim(appln_id) != '' AND projectid IS NOT NULL AND trim(projectid) != '';
