COPY (SELECT row_to_json(t) FROM (
	SELECT
		'20|ec__________::'||MD5(substring(id from 15)) AS "orgId",
	     substring(id from 15)                          AS "id",
	     'ec:PIC'                                       AS "type"
	FROM dsm_organizations
	WHERE id LIKE 'corda%'
) t) TO STDOUT;
