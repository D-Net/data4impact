COPY (SELECT row_to_json(t) FROM (
	SELECT
		'40|'||substring(id from 1 for 12)||'::'||MD5(substring(id from 15)) AS "projectId",
	    code                                                                 AS "id",
	    'ec:grant_id'                                                        AS "type"
	FROM projects
	WHERE id LIKE 'corda%'
) t) TO STDOUT;
