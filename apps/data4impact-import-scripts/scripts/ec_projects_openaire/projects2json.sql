COPY (SELECT row_to_json(t) FROM (SELECT
	'40|'||substring(p.id from 1 for 12)||'::'||MD5(substring(p.id from 15)) AS "id",
	p.title                                                                  AS "title",
	p.acronym                                                                AS "acronym",
	p.call_identifier                                                        AS "callId",
	split_part(pf.funding, '::', 2)                                          AS "funder",
	split_part(pf.funding, '::', 3)                                          AS "fundingLevel0",
	split_part(pf.funding, '::', 4)                                          AS "fundingLevel1",
	split_part(pf.funding, '::', 5)                                          AS "fundingLevel2",
	p.startdate                                                              AS "startDate",
	p.enddate                                                                AS "endDate",
	p.websiteurl                                                             AS "websiteUrl",
	p.keywords                                                               AS "keywords",
	p.contracttypescheme||':'||p.contracttypeclass                           AS "contractType",
	p.ec_sc39                                                                AS "ecSc39",
	p.oa_mandate_for_publications                                            AS "oaMandateForPublications",
	p.ec_article29_3                                                         AS "ecArticle29_3"
FROM 
	projects p 
	LEFT OUTER JOIN project_fundingpath pf ON (pf.project = p.id)
WHERE 
	p.id LIKE 'corda%'
) t) TO STDOUT;
