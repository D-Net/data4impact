#!/bin/bash

BASEDIR=/tmp/ecProjectsOpenaire

echo "Saving files in $BASEDIR ..."

rm -rf $BASEDIR
mkdir $BASEDIR

psql -h postgresql.services.openaire.eu -U dnet dnet_openaireplus -f projects2json.sql | sed 's/\\\\/\\/g' > $BASEDIR/project.json
psql -h postgresql.services.openaire.eu -U dnet dnet_openaireplus -f orgs2json.sql     | sed 's/\\\\/\\/g' > $BASEDIR/organization.json
psql -h postgresql.services.openaire.eu -U dnet dnet_openaireplus -f projOrg2json.sql  | sed 's/\\\\/\\/g' > $BASEDIR/projectOrganization.json
psql -h postgresql.services.openaire.eu -U dnet dnet_openaireplus -f orgOtherId2json.sql  | sed 's/\\\\/\\/g' > $BASEDIR/organizationOtherId.json
psql -h postgresql.services.openaire.eu -U dnet dnet_openaireplus -f projOtherId2json.sql | sed 's/\\\\/\\/g' > $BASEDIR/projectOtherId.json

echo Done.
