COPY (SELECT row_to_json(t) FROM (SELECT
	'20|ec__________::'||MD5(substring(o.id from 15)) AS "id",
	o.legalname                                       AS "name",
	o.legalshortname                                  AS "shortName",
	o.country                                         AS "country",
	o.websiteurl                                      AS "url",
	o.ec_legalbody                                    AS "ecLegalBody",
	o.ec_legalperson                                  AS "ecLegalPerson",
	o.ec_nonprofit                                    AS "ecNonProfit",
	o.ec_researchorganization                         AS "ecResearchOrganization",
	o.ec_highereducation                              AS "ecHigherEducation",
	o.ec_internationalorganizationeurinterests        AS "ecInternationalOrganizationEurInterests",
	o.ec_internationalorganization                    AS "ecInternationalOrganization",
	o.ec_enterprise                                   AS "ecEnterprise",
	o.ec_smevalidated                                 AS "ecSmeValidated",
	o.ec_nutscode                                     AS "ecNutsCode"
FROM 
	dsm_organizations o 
	LEFT OUTER JOIN project_organization po ON (po.resporganization = o.id)
WHERE 
	o.id LIKE 'corda%'
) t) TO STDOUT;





 