Paolo, Vilius, all

Last week I was in London attending a "special" event for publishers and I
had the opportunity to meet a guy from the Strategic Initiatives dep. of
Crossref who pointed me out the events API
(https://www.crossref.org/services/event-data/). Such API links
publications to several external sources including Patents, Twitter,
Wikipedia, Reddit, StackExchange, Wordpress etc.
Running some queries on their db we saw that for Twitter they do have data
for more than a year.
For patents they are based on Gambia Lens (https://www.lens.org/) -and
they do have links from patents to pubs-. Unfortunately for some reason,
Gambia uploaded data only once and then stopped. They will talk to them to
see what has happened.
In every case, I think that such API is very useful both for D4I and OA,
and we should have a look and possibly integrate such data the soonest
possible.

All the best,

Omiros