#!/bin/bash

#jsonPatents=../../orig/patents/patents.json
#jsonFulltexts=../../orig/patents/patents_txt.json

jsonPatents=../../orig/patents/patents_update.json
jsonFulltexts=../../orig/patents/patents_update_txt.json

echo
echo "Patents Import:"

#--------------------------------
echo " - Recreating the patents database"
dropdb patents --if-exists
createdb patents
psql patents -f schema.sql

#--------------------------------
inputJsonPatentsFile="$(cd "$(dirname "$jsonPatents")"; pwd -P)/$(basename "$jsonPatents")"
echo " - Importing json $inputJsonPatentsFile"
psql patents -c "copy patents_json from '$inputJsonPatentsFile' csv quote e'\x01' delimiter e'\x02'"

#--------------------------------
inputJsonFulltextsFile="$(cd "$(dirname "$jsonFulltexts")"; pwd -P)/$(basename "$jsonFulltexts")"
echo " - Importing json $jsonFulltexts"
psql patents -c "copy patents_text_json from '$inputJsonFulltextsFile' csv quote e'\x01' delimiter e'\x02'"

#--------------------------------
echo " - Refreshing views"
psql patents -c "REFRESH MATERIALIZED VIEW document"
psql patents -c "REFRESH MATERIALIZED VIEW doc_fulltext"
psql patents -c "REFRESH MATERIALIZED VIEW doc_other_identifier"
psql patents -c "REFRESH MATERIALIZED VIEW project"
psql patents -c "REFRESH MATERIALIZED VIEW doc_project"

#--------------------------------
echo " - Generating json files"
rm  -f ../../jsonfiles/patents/*.json
psql patents -c "COPY (SELECT row_to_json(t) FROM (SELECT * FROM document            ) t) TO STDOUT" | sed 's/\\\\/\\/g' > ../../jsonfiles/patents/document.json
psql patents -c "COPY (SELECT row_to_json(t) FROM (SELECT * FROM doc_fulltext        ) t) TO STDOUT" | sed 's/\\\\/\\/g' > ../../jsonfiles/patents/doc_fulltext.json
psql patents -c "COPY (SELECT row_to_json(t) FROM (SELECT * FROM doc_other_identifier) t) TO STDOUT" | sed 's/\\\\/\\/g' > ../../jsonfiles/patents/doc_other_identifier.json

# COMMENT THE FOLLOWING LINES IF THE PATENTS ARE NOT RELATED TO FP7
psql patents -c "COPY (SELECT row_to_json(t) FROM (SELECT * FROM project             ) t) TO STDOUT" | sed 's/\\\\/\\/g' > ../../jsonfiles/patents/project.json
psql patents -c "COPY (SELECT row_to_json(t) FROM (SELECT * FROM doc_project         ) t) TO STDOUT" | sed 's/\\\\/\\/g' > ../../jsonfiles/patents/doc_project.json

echo "Done."
echo
