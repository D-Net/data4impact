CREATE TABLE patents_json (
	json	text
);

CREATE TABLE patents_text_json (
	json	text
);

CREATE MATERIALIZED VIEW document AS SELECT
	'50|patents_____::'||MD5(lower(trim(p->>'LocalID'))) AS "id",
	p->>'Title'                                          AS "title",
	p->>'Abstract'                                       AS "abstractText",
	p->>'Type'                                           AS "type",
	p->>'PubYear'                                        AS "pubYear",
	'patent repo'::text                                  AS "repository"
FROM (SELECT replace(json,'\\','\"')::json AS p FROM patents_json) a;


CREATE MATERIALIZED VIEW doc_fulltext AS SELECT
	'50|patents_____::'||MD5(lower(trim(p->>'LocalID'))) AS "docId",
	trim(p->>'text')                                     AS "fulltext"
FROM (SELECT replace(json,'\\"','\"')::json AS p FROM patents_text_json) a
WHERE length(trim(p->>'text')) > 0;


CREATE MATERIALIZED VIEW doc_other_identifier AS SELECT
	'50|patents_____::'||MD5(lower(trim(p->>'LocalID'))) AS "docId",
	trim(p->>'LocalID')                                  AS "id",
	'patent'::text                                       AS "type"
FROM (SELECT replace(json,'\\"','\"')::json AS p FROM patents_json) a;

CREATE MATERIALIZED VIEW project AS SELECT
	'50|MOCK_PROJECT::'||MD5('EC_FP7')::text AS "id",
	'MOCK PROJECT'::text                     AS "title",
	'EC'::text                               AS "funder",
	'FP7'::text                              AS "fundingLevel0";	

CREATE MATERIALIZED VIEW doc_project AS SELECT
	'50|patents_____::'||MD5(lower(trim(p->>'LocalID'))) AS "docId",
	'50|MOCK_PROJECT::'||MD5('EC_FP7')                   AS "projectId"
FROM (SELECT replace(json,'\\"','\"')::json AS p FROM patents_json) a;
