CREATE TABLE projects (
	swecris_info                       text,
	doi                                text,
	final_reports                      text,
	Organization_short                 text,
	Organization_long                  text,
	dnr                                text,
	people_project_leaders_0_surname   text,
	people_project_leaders_0_firstname text,
	organizations_coordinating_en      text,
	type_of_awards                     text,
	dates_start_date                   text,
	dates_end_date                     text,
	title_en                           text,
	abstract_en                        text,
	intrascientific_report_en          text,
	popular_report_sv                  text,
	tags_0_en                          text,
	doi_list                           text,
	total_funding                      numeric
);

