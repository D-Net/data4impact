COPY (SELECT row_to_json(t) FROM (SELECT
	'40|'||rpad(lower(organization_short),12,'_')||'::'||MD5(dnr) AS "projectId",
	dnr                                                           AS "id",
	lower(organization_short)||':grant_id'                        AS "type"
FROM projects 
) t) TO STDOUT;
