COPY (SELECT row_to_json(t) FROM (
	SELECT
	 	token AS "id",
	 	'doi' AS "type"
	FROM projects p , unnest(string_to_array(p.doi_list, ',')) s(token) 
	WHERE token IS NOT NULL
) t) TO STDOUT;