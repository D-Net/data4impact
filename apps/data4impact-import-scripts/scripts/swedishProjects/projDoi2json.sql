COPY (SELECT row_to_json(t) FROM (
	SELECT
		'40|'||rpad(lower(organization_short),12,'_')||'::'||MD5(dnr) AS "projectId",
	 	token                                                         AS "docId",
	 	'doi'                                                         AS "docIdType"
	FROM projects p , unnest(string_to_array(p.doi_list, ',')) s(token) 
	WHERE token IS NOT NULL
) t) TO STDOUT;
