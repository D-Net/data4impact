COPY (SELECT row_to_json(t) FROM (SELECT distinct
	'20|swedish_orgs::'||MD5(lower(organizations_coordinating_en)) AS "id",
	organizations_coordinating_en                                  AS "name",
	'SE'                                                           AS "country"
FROM projects
) t) TO STDOUT;
