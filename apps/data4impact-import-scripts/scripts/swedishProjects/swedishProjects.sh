#!/bin/bash

csv=/tmp/180626-swe_proj_data-delivery.csv

inputCsvFile="$(cd "$(dirname "$csv")"; pwd -P)/$(basename "$csv")"

echo
echo "Swedish Projects Import:"

#--------------------------------
echo " - Recreating the swedishprojects database"
dropdb swedishprojects --if-exists;
createdb swedishprojects;
psql swedishprojects -f schema.sql
psql swedishprojects -c "COPY projects(swecris_info, doi, final_reports, Organization_short, Organization_long, dnr, people_project_leaders_0_surname, people_project_leaders_0_firstname, organizations_coordinating_en, type_of_awards, dates_start_date, dates_end_date, title_en, abstract_en, intrascientific_report_en, popular_report_sv, tags_0_en, doi_list, total_funding) FROM '$inputCsvFile' DELIMITER ',' CSV HEADER;"

#--------------------------------
echo " - Generating json files"
rm  -f ../../jsonfiles/swedishProjects/*.json
psql swedishprojects -f projects2json.sql      | sed 's/\\\\/\\/g' > ../../jsonfiles/swedishProjects/project.json
psql swedishprojects -f orgs2json.sql          | sed 's/\\\\/\\/g' > ../../jsonfiles/swedishProjects/organization.json
psql swedishprojects -f projOrg2json.sql       | sed 's/\\\\/\\/g' > ../../jsonfiles/swedishProjects/projectOrganization.json
psql swedishprojects -f projOtherIds2json.sql  | sed 's/\\\\/\\/g' > ../../jsonfiles/swedishProjects/projectOtherId.json
psql swedishprojects -f docOtherId2json.sql    | sed 's/\\\\/\\/g' > ../../jsonfiles/swedishProjects/docotherid.json
psql swedishprojects -f projDoi2json.sql       | sed 's/\\\\/\\/g' > ../../jsonfiles/swedishProjects/projectdocotherid.json

echo "Done."
echo

