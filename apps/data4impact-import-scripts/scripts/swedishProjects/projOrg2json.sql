COPY (SELECT row_to_json(t) FROM (SELECT
	'40|'||rpad(lower(organization_short),12,'_')||'::'||MD5(dnr)  AS "projectId",
	'20|swedish_orgs::'||MD5(lower(organizations_coordinating_en)) AS "orgId",
	'coordinator'                       AS "role",
	people_project_leaders_0_firstname          AS "contactFirstNames",
	people_project_leaders_0_surname           AS "contactLastNames"
FROM projects
) t) TO STDOUT;

