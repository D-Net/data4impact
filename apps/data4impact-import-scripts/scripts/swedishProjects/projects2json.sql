COPY (SELECT row_to_json(t) FROM (SELECT
	'40|'||rpad(lower(organization_short),12,'_')||'::'||MD5(dnr) AS "id",
	title_en                                                      AS "title",
	organization_short                                            AS "funder",
	type_of_awards                                                AS "fundingLevel0",
	dates_start_date                                              AS "startDate",
	dates_end_date                                                AS "endDate",
	abstract_en                                                   AS "abstractText",
	tags_0_en                                                     AS "keywords",
	total_funding                                                 AS "contribution",
	'SEK'::text                                                   AS "currency"
FROM projects 
) t) TO STDOUT;


--	intrascientific_report_en          text,
--	popular_report_sv                  text,
--	doi_list                           text,
--	total_funding                      text