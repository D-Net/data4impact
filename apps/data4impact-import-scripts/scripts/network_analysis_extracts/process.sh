#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

table=network_analysis_metrics

echo "Recrreating table $table"
psql -h localhost -U d4i data4impact -c "DROP TABLE IF EXISTS $table;"
psql -h localhost -U d4i data4impact -c "CREATE TABLE $table (betweenness_centrality double precision,closeness_centrality double precision,degree_centrality double precision,eccentricity_centrality double precision,eigenvector_centrality double precision,farness_centrality double precision,pic text,name text, icd int, period text, orgid text);"
echo

for icd in {1..19}
do
	if [ -d "$DIR/$icd" ]; then
 		cd "$DIR/$icd"
		for csv in *.csv
		do
			y1=$(echo $csv | cut -c1-4)
			y2=$(expr $y1 + 1)
			period="$y1-$y2"
			
			echo "Processing file $DIR/$icd/$csv..."
			
			if grep --quiet eccentricity_centrality  "$DIR/$icd/$csv"; then
				psql -h localhost -U d4i data4impact -c "COPY $table (betweenness_centrality,closeness_centrality,degree_centrality,eccentricity_centrality,eigenvector_centrality,farness_centrality,pic,name) FROM '$DIR/$icd/$csv' CSV HEADER;"
			else
				psql -h localhost -U d4i data4impact -c "COPY $table (betweenness_centrality,closeness_centrality,degree_centrality,eigenvector_centrality,farness_centrality,pic,name) FROM '$DIR/$icd/$csv' CSV HEADER;"
			fi
					

			psql -h localhost -U d4i data4impact -c "UPDATE $table SET (icd,period) = ($icd,'$period') WHERE icd IS NULL;" 
			echo;

		done
		
	fi
done


echo "Fixing values..."
psql -h localhost -U d4i data4impact -c "UPDATE $table SET pic = replace(pic, '.0', '') WHERE pic IS NOT NULL;"
psql -h localhost -U d4i data4impact -c "UPDATE $table SET orgid = '20|ec__________::'||MD5(pic) WHERE pic IS NOT NULL;"

echo

echo "Done."
echo
echo

