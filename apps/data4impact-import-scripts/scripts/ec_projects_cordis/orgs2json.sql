COPY (SELECT row_to_json(t) FROM (SELECT
	'20|ec__________::'||MD5(orgid) AS "id",
	MAX(orgname)                    AS "name",
	MAX(orgshortname)               AS "shortName",
	MAX(country)                    AS "country",
	MAX(street)                     AS "street",
	MAX(city)                       AS "city",
	MAX(postcode)                   AS "postCode",
	MAX(organizationurl)            AS "url"
FROM participants
WHERE orgid IS NOT NULL
GROUP BY orgid
) t) TO STDOUT;
