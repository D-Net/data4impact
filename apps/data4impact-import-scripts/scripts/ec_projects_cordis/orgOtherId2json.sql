COPY (SELECT row_to_json(t) FROM (
	SELECT
		'20|ec__________::'||MD5(orgid) AS "orgId",
	     orgid                          AS "id",
	     'ec:PIC'                       AS "type"
	FROM participants
	WHERE orgid IS NOT NULL
	GROUP BY orgid 
) t) TO STDOUT;
