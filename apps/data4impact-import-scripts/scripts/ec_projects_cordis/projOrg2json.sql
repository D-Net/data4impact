COPY (SELECT row_to_json(t) FROM (
	SELECT
		CASE 
			WHEN fundingprogram='FP7'   THEN '40|corda_______::'||MD5(projectid)
			WHEN fundingprogram='H2020' THEN '40|corda__h2020::'||MD5(projectid)
			ELSE                             '40|unknown_____::'||MD5(projectid)
		END                             AS "projectId",
		'20|ec__________::'||MD5(orgid) AS "orgId",
		MAX(role)                       AS "role",
		MAX(activitytype)               AS "activityType",
		MAX(endofparticipation)         AS "endOfParticipation",
		MAX(eccontribution)             AS "ecContribution",
		MAX(contacttype)                AS "contactType",
		MAX(contacttitle)               AS "contactTitle",
		MAX(contactfirstnames)          AS "contactFirstNames",
		MAX(contactlastnames)           AS "contactLastNames",
		MAX(contactfunction)            AS "contactFunction",
		MAX(contacttelephonenumber)     AS "contactTelephoneNumber",
		MAX(contactfaxnumber)           AS "contactFaxNumber",
		MAX(contactform)                AS "contactForm"
	FROM participants
	WHERE orgid IS NOT NULL AND projectid IS NOT NULL
	GROUP BY orgid, projectid, fundingprogram
) t) TO STDOUT;


