COPY (SELECT row_to_json(t) FROM (
	SELECT
		CASE 
			WHEN fundingprogram='FP7'   THEN '40|corda_______::'||MD5(projectid)
			WHEN fundingprogram='H2020' THEN '40|corda__h2020::'||MD5(projectid)
			ELSE                             '40|unknown_____::'||MD5(projectid)
		END           AS "projectId",
	    projectid     AS "id",
	    'ec:grant_id' AS "type"
	FROM participants
	WHERE projectid IS NOT NULL
	GROUP BY projectid, fundingprogram

	UNION ALL

	SELECT
		CASE 
			WHEN fundingprogram='FP7'   THEN '40|corda_______::'||MD5(projectid)
			WHEN fundingprogram='H2020' THEN '40|corda__h2020::'||MD5(projectid)
			ELSE                             '40|unknown_____::'||MD5(projectid)
		END             AS "projectId",
	    MAX(projectrcn) AS "id",
	    'ec:RCN'        AS "type"
	FROM participants
	WHERE projectid IS NOT NULL AND projectrcn IS NOT NULL
	GROUP BY projectid, fundingprogram
) t) TO STDOUT;
