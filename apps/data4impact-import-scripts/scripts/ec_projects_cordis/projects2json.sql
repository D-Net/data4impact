COPY (SELECT row_to_json(t) FROM (
	SELECT
		CASE 
			WHEN fundingprogram='FP7'   THEN '40|corda_______::'||MD5(projectid)
			WHEN fundingprogram='H2020' THEN '40|corda__h2020::'||MD5(projectid)
			ELSE                             '40|unknown_____::'||MD5(projectid)
		END                 AS "id",
		MAX(projectacronym) AS "acronym",
		'EC'                AS "funder",
		fundingprogram      AS "fundingLevel0"
	FROM participants
	WHERE projectid IS NOT NULL
	GROUP BY
		projectid,
		fundingprogram
) t) TO STDOUT;
