#!/bin/bash

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

tmp="/tmp/tempfile.sql"
rm -f "$tmp"

echo "DELETE FROM project_portfolio;" >> "$tmp"

for f in `ls /data/d4i/project_portfolios/november2018/D4I_Analytics_ARC_Release04_WP52_31Nov2018_fixed/FP7_*.json`
do
	id=$(jq .administrative_data.project_id "$f" | tr -d '"')
	echo -n "INSERT INTO project_portfolio(projectid, portfolio) VALUES ('40|corda_______::'||MD5('$id'), '" >> "$tmp"
	cat "$f" | gzip -c | base64 | tr -d '\n' >> "$tmp"
	echo "');" >> "$tmp"
done

for f in `ls /data/d4i/project_portfolios/november2018/D4I_Analytics_ARC_Release04_WP52_31Nov2018_fixed/H2020_*.json`
do
	id=$(jq .administrative_data.project_id "$f" | tr -d '"')
	echo -n "INSERT INTO project_portfolio(projectid, portfolio) VALUES ('40|corda__h2020::'||MD5('$id'), '" >> "$tmp"
	cat "$f" | gzip -c | base64 | tr -d '\n' >> "$tmp"
	echo "');" >> "$tmp"
done

echo "Inserting file: $tmp"

#psql data4impact -f "$tmp"

IFS=$SAVEIFS
