CREATE TABLE pp_metrics (
	id                             text PRIMARY KEY,
	eu_contribution                numeric,
	number_of_innovations          integer,
	number_of_companies_founded    integer,
	number_of_patents              integer,
	number_of_projects             integer,
	number_of_pubmed_publications  integer,
	number_of_rest_publications    integer,
	number_of_segments             integer,
	total_cost                     numeric
);

CREATE TABLE pp_countries_cooccurrences (
	funding         text REFERENCES pp_metrics(id),
	country1        text,
	country2        text,
	number          integer,
	PRIMARY KEY (funding, country1, country2)
);

CREATE TABLE pp_eu_contribution_per_country (
	funding         text REFERENCES pp_metrics(id),
	country         text,
	contribution    numeric,
	PRIMARY KEY (funding, country)
);

CREATE TABLE pp_eu_contribution_per_participant_sector (
	funding         text REFERENCES pp_metrics(id),
	sector          text,
	contribution    numeric,
	PRIMARY KEY (funding, sector)
);

CREATE TABLE pp_eu_contribution_per_research_area (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	contribution    numeric,
	PRIMARY KEY (funding, area)
);

CREATE TABLE pp_eu_contribution_per_research_area_over_time (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	year            integer,
	contribution    numeric,
	PRIMARY KEY (funding, area, year)

);

CREATE TABLE pp_eu_contribution_per_year (
	funding         text REFERENCES pp_metrics(id),
	year            integer,
	contribution    numeric,
	PRIMARY KEY (funding, year)
);

CREATE TABLE pp_number_of_innovations_per_type (
	funding         text REFERENCES pp_metrics(id),
	type            text,
	number          integer,
	PRIMARY KEY (funding, type)
);

CREATE TABLE pp_number_of_innovations_per_type_per_country (
	funding         text REFERENCES pp_metrics(id),
	type            text,
	country         text,
	number          integer,
	PRIMARY KEY (funding, type, country)
);

CREATE TABLE pp_number_of_innovations_per_type_per_research_area (
	funding         text REFERENCES pp_metrics(id),
	type            text,
	area            text,
	number          integer,
	PRIMARY KEY (funding, type, area)
);

CREATE TABLE pp_number_of_patents_per_research_area (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	number          integer,
	PRIMARY KEY (funding, area)
);

CREATE TABLE pp_number_of_projects_per_research_area (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	number          integer,
	PRIMARY KEY (funding, area)
);

CREATE TABLE pp_number_of_pubmed_publications_per_country (
	funding         text REFERENCES pp_metrics(id),
	country         text,
	number          integer,
	PRIMARY KEY (funding, country)
);

CREATE TABLE pp_number_of_pubmed_publications_per_journal (
	funding         text REFERENCES pp_metrics(id),
	journal         text,
	number          integer,
	PRIMARY KEY (funding, journal)
);

CREATE TABLE pp_number_of_pubmed_publications_per_journal_per_research_area (
	funding         text REFERENCES pp_metrics(id),
	journal         text,
	area            text,
	number          integer,
	PRIMARY KEY (funding, journal, area)
);

CREATE TABLE pp_number_of_pubmed_publications_per_journal_per_year (
	funding         text REFERENCES pp_metrics(id),
	journal         text,
	year            integer,
	number          integer,
	PRIMARY KEY (funding, journal, year)
);

CREATE TABLE pp_number_of_pubmed_publications_per_research_area (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	number          integer,
	PRIMARY KEY (funding, area)
);

CREATE TABLE pp_number_of_pubmed_publications_per_year (
	funding         text REFERENCES pp_metrics(id),
	year            integer,
	number          integer,
	PRIMARY KEY (funding, year)
);

-- IT IS EQUIVALENT TO pp_number_of_pubmed_publications_per_journal_per_year --
CREATE TABLE pp_number_of_pubmed_publications_per_year_per_journal (
	funding         text REFERENCES pp_metrics(id),
	journal         text,
	year            integer,
	number          integer,
	PRIMARY KEY (funding, journal, year)
);

CREATE TABLE pp_number_of_rest_publications_per_research_area (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	number          integer,
	PRIMARY KEY (funding, area)
);

CREATE TABLE pp_number_of_rest_publications_per_year (
	funding         text REFERENCES pp_metrics(id),
	year            integer,
	number          integer,
	PRIMARY KEY (funding, year)
);

CREATE TABLE pp_research_areas_cooccurrences (
	funding         text REFERENCES pp_metrics(id),
	area1           text,
	area2           text,
	number          integer,
	PRIMARY KEY (funding, area1, area2)
);

CREATE TABLE pp_research_areas_to_icd10 (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	icd10           text,
	PRIMARY KEY (funding, area)
);

CREATE TABLE pp_total_cost_per_research_area (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	cost            numeric,
	PRIMARY KEY (funding, area)
);

CREATE TABLE pp_total_cost_per_research_area_over_time (
	funding         text REFERENCES pp_metrics(id),
	area            text,
	year            integer,
	cost            numeric,
	PRIMARY KEY (funding, area, year)
);

CREATE TABLE pp_total_cost_per_year (
	funding         text REFERENCES pp_metrics(id),
	year            integer,
	cost            numeric,
	PRIMARY KEY (funding, year)
);

