CREATE TABLE data (
	company_id         text,
	prediction_revised float,
	site_url           text,
	source             text,
	text_clean_gentle  text,
	text_clean_strong  text,
	text_is_duplicated boolean
);

