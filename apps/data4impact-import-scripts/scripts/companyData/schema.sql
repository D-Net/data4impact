CREATE TABLE companymetrics (
	orgid                 text,
	data_gathered         varchar(5),
    tangible_pre_market   int,
    tangible_market       int,
    intangible_pre_market int,
    intangible_market     int,
    innovation            varchar(5)
);


