COPY (SELECT row_to_json(t) FROM (
	SELECT
		'20|ec__________::'||MD5(orgid) AS "orgId",
		(LOWER(data_gathered)='yes')    AS "dataGathered",
	    tangible_pre_market             AS "tangiblePreMarket",
	    tangible_market                 AS "tangibleMarket",
	    intangible_pre_market           AS "intangiblePreMarket",
	    intangible_market               AS "intangibleMarket",
	    CASE 
			WHEN innovation='0' THEN false
			WHEN innovation='1' THEN true
			ELSE                     NULL
		END                             AS "innovation"
	FROM companymetrics
) t) TO STDOUT;