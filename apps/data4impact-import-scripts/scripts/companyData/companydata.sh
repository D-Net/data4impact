#!/bin/bash

detailsFile=../../orig/CompanyData/D4I_companies_summary.txt

workdir=/tmp/companydata
rm -rf "$workdir" && mkdir "$workdir"

echo
echo "CompanyData Import:"

#--------------------------------
echo " - Generating csv files"
csvDetails="$workdir/details.csv"
cat $detailsFile | jq 'to_entries' | jq 'map([.key, .value."data gathered?", .value."tangible + pre_market", .value."tangible + market", .value."intangible + pre_market", .value."intangible + market", .value."innovation?"])' | jq .[] | jq -r @csv > $csvDetails

#--------------------------------
echo " - Recreating the companydata database"
dropdb companydata --if-exists;
createdb companydata;
psql companydata -f schema.sql

if [[ -f "$csvDetails" ]]; then
	echo " - Importing details: $csvDetails"
	psql companydata -c "COPY companymetrics(orgid, data_gathered, tangible_pre_market, tangible_market, intangible_pre_market, intangible_market, innovation) FROM '$csvDetails' CSV;"
else
   	echo " - Invalid file: $csvDetails"
fi

#--------------------------------
echo " - Generating json files"
rm  -f ../../jsonfiles/companydata/*.json
psql companydata -f metrics2json.sql     | sed 's/\\\\/\\/g' > ../../jsonfiles/companydata/orgCompanyMetrics.json


echo "Done."
echo

