COPY (SELECT row_to_json(t) FROM (
	SELECT
		'20|ec__________::'||MD5(company_id) AS "orgId",
		prediction_revised                   AS "predictionRevised",
		site_url                             AS "siteUrl",
		source                               AS "source",
		text_clean_gentle                    AS "textCleanGentle",
		text_clean_strong                    AS "textCleanStrong",
		text_is_duplicated                   AS "duplicated"
	FROM data
) t) TO STDOUT;
