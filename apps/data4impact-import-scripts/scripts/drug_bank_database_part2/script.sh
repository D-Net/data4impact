#!/bin/bash

excelFile="../../orig/drug_bank_database/DB_Publication_project_links.xlsx"

workdir=/tmp/drugbank_part2
rm -rf "$workdir" && mkdir "$workdir"

echo
echo "Links from drugbank db Import:"


#--------------------------------
echo " - Generating csv file"
csv="$workdir/drugbank.csv"
xlsx2csv -c UTF-8 "$excelFile" > $csv

#--------------------------------
echo " - Recreating the drugbank database"
dbname=drugbank_p2

dropdb $dbname --if-exists;
createdb $dbname;
psql $dbname -f schema.sql

if [[ -f "$csv" ]]; then
	echo " - Importing data: $csv"
	psql $dbname -c "COPY data(doi,d_b_id,pmcid,pmid,drug_substance,ec_project_acronym,ec_project_code,funding_scheme,match_type) FROM '$csv' CSV HEADER;"
else
   	echo " - Invalid file: $csv"
fi

#--------------------------------
echo " - Generating json files"
rm  -f ../../jsonfiles/drug_bank_database/*.json
psql $dbname -f docOtherId2json.sql      | sed 's/\\\\/\\/g' > ../../jsonfiles/drug_bank_database_part2/docotherid.json
psql $dbname -f projDocOtherIds2json.sql | sed 's/\\\\/\\/g' > ../../jsonfiles/drug_bank_database_part2/projectdocotherid.json

echo "Done."
echo
