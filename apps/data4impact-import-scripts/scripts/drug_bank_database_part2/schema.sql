CREATE TABLE data (
	doi                 text,
	d_b_id              text,
	pmcid               text,
	pmid                text,
	drug_substance      text,
	ec_project_acronym  text,
	ec_project_code     text,
	funding_scheme      text,
	match_type          text
);
