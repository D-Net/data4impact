# MANUAL STEPS FOR clinical trials

1) cd /data/ftp/d4i/clinical_trials

2) Recreate the table in the DB using

DROP TABLE clinical_trials;

CREATE TABLE clinical_trials (
	doi             text,
	trial_number    text,
	trial_registry  text
);

4) Insert data:

COPY clinical_trials(doi,trial_number,trial_registry) FROM '/data/ftp/d4i/clinical_trials/clintrial.txt' DELIMITER E'\t';
DELETE FROM clinical_trials where doi = 'pub-with-clin-trial';
