#!/bin/bash


#detailsFile=../../orig/guidelines/guidelines.json
detailsFile=/tmp/guidelines.json

workdir=/tmp/guidelines
rm -rf "$workdir" && mkdir "$workdir"

echo
echo "Guidelines Import:"

#--------------------------------
echo " - Generating csv files"
csvGuidelines="$workdir/guidelines.csv"
csvRels="$workdir/rels.csv"
csvAllRels="$workdir/allRels.csv"

cat $detailsFile | jq 'map([.LocalID, .Type, .Title, .PubYear, .Originator, .ProviderCollection, .Abstract, .PMID, .DOI, .PMCID])' | jq .[] | jq -r @csv > $csvGuidelines
cat $detailsFile | jq -r '.[] | .LocalID as $id | (.MatchedReferences | map([$id, (.PMID + ""), (.PMCID + ""), (.DOI + "") , ( .Funders | map(.+"#") | add | . + "" )  ]) )[] | @csv' > $csvRels
cat $detailsFile | jq -r '.[] | .LocalID as $id | (.AllReferences | map([$id, .]) )[] | @csv' > $csvAllRels

#--------------------------------
echo " - Recreating the guidelines database"
dropdb guidelines --if-exists;
createdb guidelines;
psql guidelines -f schema.sql

if [[ -f "$csvGuidelines" ]]; then
	echo " - Importing guidelines: $csvGuidelines"
	psql guidelines -c "COPY guidelines(id, gtype, title, year, orig, collection, abstract, pmid, doi, pmcid) FROM '$csvGuidelines' CSV;"
else
   	echo " - Invalid file: $csvGuidelines"
fi

if [[ -f "$csvRels" ]]; then
	echo " - Importing rels: $csvRels"
	psql guidelines -c "COPY relations(gid, pmid, pmcid, doi, funder) FROM '$csvRels' CSV;"
else
   	echo " - Invalid file: $csvRels"
fi

if [[ -f "$csvAllRels" ]]; then
	echo " - Importing all rels: $csvAllRels"
	psql guidelines -c "COPY allrefs(gid, rel) FROM '$csvAllRels' CSV;"
else
   	echo " - Invalid file: $csvAllRels"
fi


#--------------------------------
echo " - Generating json files"
rm  -f ../../jsonfiles/guidelines/*.json
psql guidelines -f document2json.sql           | sed 's/\\\\/\\/g' > ../../jsonfiles/guidelines/document.json
psql guidelines -f docOtherId2json.sql         | sed 's/\\\\/\\/g' > ../../jsonfiles/guidelines/docotherid.json
psql guidelines -f projects2json.sql           | sed 's/\\\\/\\/g' > ../../jsonfiles/guidelines/project.json
psql guidelines -f projDocOtherIds2json.sql    | sed 's/\\\\/\\/g' > ../../jsonfiles/guidelines/projectdocotherid.json
psql guidelines -f docDocumentOtherId2json.sql | sed 's/\\\\/\\/g' > ../../jsonfiles/guidelines/docDocumentOtherId.json

#--------------------------------
echo " - Importing final files"
cd  ../../jsonfiles/guidelines

echo "Done."
echo


