COPY (SELECT row_to_json(t) FROM (
	SELECT
		'50|guidelines__::'||MD5(gid) AS "docId1",
	 	pmcid                         AS "docId2",
	 	'pmcid'                       AS "docId2Type",
	 	'guidelines_matched'          AS "relType"
	FROM relations
	WHERE pmcid IS NOT NULL AND pmcid != ''
	
	UNION
	
	SELECT
		'50|guidelines__::'||MD5(gid) AS "docId1",
	 	pmid                          AS "docId2",
	 	'pmid'                        AS "docId2Type",
	 	'guidelines_matched'          AS "relType"
	FROM relations
	WHERE pmid IS NOT NULL AND pmid != ''
	
	UNION

	SELECT
	'50|guidelines__::'||MD5(gid) AS "docId1",
	doi 	                        AS "docId2",
	'doi'                        	AS "docId2Type",
	'guidelines_matched'          AS "relType"
	FROM relations
	WHERE doi IS NOT NULL AND doi != ''

	UNION
	
	SELECT
		'50|guidelines__::'||MD5(gid) AS "docId1",
		rel                           AS "docId2",
		'pmid'                        AS "docId2Type",
		'guidelines_all'              AS "relType"
	FROM allrefs
	WHERE rel IS NOT NULL AND rel != ''
) t) TO STDOUT;
