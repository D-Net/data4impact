CREATE TABLE guidelines (
	id         text,
	gtype      text,
	title      text,
	year       text,
	orig       text,
	collection text,
	abstract   text,
	pmid       text,
	doi        text,
	pmcid      text
);

CREATE TABLE relations (
	gid    text,
	pmid   text, 
	pmcid  text,
	doi		 text,
	funder text
);

CREATE TABLE allrefs (
	gid    text,
	rel    text
);
