COPY (SELECT row_to_json(t) FROM (
	SELECT
		'50|guidelines__::'||MD5(g.id)          AS "id",
		g.title                                 AS "title",
		g.abstract                              AS "abstractText",
		g.gtype                                 AS "type",
		g.year                                  AS "pubYear",
		g.orig                                  AS "repository",
		g.collection                            AS "collection"
	FROM 
		guidelines	g
) t) TO STDOUT;
