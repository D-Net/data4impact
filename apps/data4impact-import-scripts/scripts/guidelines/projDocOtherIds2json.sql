COPY (SELECT row_to_json(t) FROM (
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	pmcid                            AS "docId",
	 	'pmcid'                          AS "docIdType"
	FROM (select * from (select pmid, pmcid, unnest(string_to_array(funder, '#')) as funder from relations) as t where length(t.funder) > 0) r
	WHERE pmcid IS NOT NULL AND pmcid != ''
	
	UNION ALL
	
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	pmid                             AS "docId",
	 	'pmid'                           AS "docIdType"
	FROM (select * from (select pmid, pmcid, unnest(string_to_array(funder, '#')) as funder from relations) as t where length(t.funder) > 0) r
	WHERE pmid IS NOT NULL AND pmid != ''

) t) TO STDOUT;


