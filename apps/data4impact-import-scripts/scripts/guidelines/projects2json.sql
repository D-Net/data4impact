COPY (SELECT row_to_json(t) FROM (SELECT distinct
	'40|MOCK_PROJECT::'||MD5(funder) AS "id",
	'MOCK PROJECT'                   AS "title",
	funder                           AS "funder"
FROM
	(SELECT DISTINCT unnest(string_to_array(funder, '#')) AS funder FROM relations ) r WHERE LENGTH(r.funder) > 0
) t) TO STDOUT;
