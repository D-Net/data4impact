Dear Claudio, Please find the json file containing the clinical guideline base data attached.

It is formatted as follows:

LocalID    [Our local guideline ID]
Type    "guideline"
Title    [Title of guideline]
PubYear   [Guideline publication year]
Originator    [Organization that created the guideline (subset of ProviderCollection)]
ProviderCollection    [Collection name]
Abstract    [Guideline abstract (from PubMed, if available (only from WHO, NICE and Cochrane))]
PMID    [PMID if available]
DOI    [DOI if available]
PMCID   [PMCID if available]
MatchedReferences: [references matched with Our set of publications as PMID (as well as PMCID and funder name)]
[All]References: [All references in each guideline]

We also have the full text for WHO, NICE and Cochrane, as well as the PDF:s for the German AWMF guidelines, but it is still uncertain how this material could be shared due to copyright issues.


FILE: /data/d4i/guidelines.json.zip
