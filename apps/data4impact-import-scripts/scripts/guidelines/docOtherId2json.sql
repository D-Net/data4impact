COPY (SELECT row_to_json(t) FROM (
	
	SELECT
		'50|guidelines__::'||MD5(id) AS "docId",
	 	id                           AS "id",
	 	'guidelineLocalID'           AS "type"
	FROM guidelines
	
	UNION ALL

	SELECT
		'50|guidelines__::'||MD5(id) AS "docId",
	 	pmcid                        AS "id",
	 	'pmcid'                      AS "type"
	FROM guidelines
	WHERE pmcid IS NOT NULL AND pmcid != ''
	
	UNION ALL
	
	SELECT
		'50|guidelines__::'||MD5(id) AS "docId",
		pmid                         AS "id",
	 	'pmid'                       AS "type"
	FROM guidelines
	WHERE pmid IS NOT NULL AND pmid != ''
	
	UNION ALL
	
	SELECT
		'50|guidelines__::'||MD5(id) AS "docId",
	 	doi                          AS "id",
	 	'doi'                        AS "type"
	FROM guidelines
	WHERE doi IS NOT NULL AND doi != ''
	
	UNION
	
	SELECT
		NULL                         AS "docId",
	 	pmcid                        AS "id",
	 	'pmcid'                      AS "type"
	FROM relations
	WHERE pmcid IS NOT NULL AND pmcid != ''
	
	UNION
	
	SELECT
		NULL                         AS "docId",
	 	pmid                         AS "id",
	 	'pmid'                       AS "type"
	FROM relations
	WHERE pmid IS NOT NULL AND pmid != ''

	UNION

	SELECT
	NULL                         AS "docId",
	doi                          AS "id",
	'doi'                        AS "type"
	FROM relations
	WHERE pmid IS NOT NULL AND doi != ''
	
	UNION
	
	SELECT
		NULL                        AS "docId",
	 	rel                         AS "id",
	 	'pmid'                      AS "type"
	FROM allrefs
	WHERE rel IS NOT NULL AND rel != ''
	
) t) TO STDOUT;
