COPY (SELECT row_to_json(t) FROM (
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	pmcid                            AS "docId",
	 	'pmcid'                          AS "docIdType"
	FROM data
	WHERE pmcid IS NOT NULL AND pmcid != ''
	
	UNION ALL
	
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	pmid                             AS "docId",
	 	'pmid'                           AS "docIdType"
	FROM data
	WHERE pmid IS NOT NULL AND pmid != ''
	
	UNION ALL
	
	SELECT
		'40|MOCK_PROJECT::'||MD5(funder) AS "projectId",
	 	doi                             AS "docId",
	 	'doi'                           AS "docIdType"
	FROM data
	WHERE doi IS NOT NULL AND doi != ''
) t) TO STDOUT;
