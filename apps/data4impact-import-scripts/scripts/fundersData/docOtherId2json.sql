COPY (SELECT row_to_json(t) FROM (
	SELECT
	 	pmcid   AS "id",
	 	'pmcid' AS "type"
	FROM data
	WHERE pmcid IS NOT NULL AND pmcid != ''
	
	UNION ALL
	
	SELECT
	 	pmid   AS "id",
	 	'pmid' AS "type"
	FROM data
	WHERE pmid IS NOT NULL AND pmid != ''
	
	UNION ALL
	
	SELECT
	 	doi   AS "id",
	 	'doi' AS "type"
	FROM data
	WHERE doi IS NOT NULL AND doi != ''
	
) t) TO STDOUT;