#!/bin/bash

excelFile="../../orig/fundersData/Funders, DOIS 31122018.xlsx" 



workdir=/tmp/funderData
rm -rf "$workdir" && mkdir "$workdir"



echo
echo "Funder Data Import:"


#--------------------------------
echo " - Generating csv file"
csv="$workdir/funderdata.csv"
xlsx2csv -c UTF-8 "$excelFile" > $csv

#--------------------------------
echo " - Recreating the funderdata database"
dropdb funderdata --if-exists;
createdb funderdata;
psql funderdata -f schema.sql

if [[ -f "$csv" ]]; then
	echo " - Importing data: $csv"
	psql funderdata -c "COPY data(funder,pmcid,pmid,source,doi) FROM '$csv' CSV HEADER;"
else
   	echo " - Invalid file: $csv"
fi


echo " - Fix funder names"
psql funderdata -c "UPDATE data SET funder='EC' WHERE funder = 'Marie Curie'"
psql funderdata -c "UPDATE data SET funder='EC' WHERE funder = 'European Research Council'"
psql funderdata -c "UPDATE data SET funder='Breast Cancer Now' WHERE funder = 'BreastCancerNow'"
psql funderdata -c "UPDATE data SET funder='Wellcome Trust' WHERE funder = 'Wellcome Trust/DBT India Alliance'"



#--------------------------------
echo " - Generating json files"
rm  -f ../../jsonfiles/funderdata/*.json
psql funderdata -f projects2json.sql        | sed 's/\\\\/\\/g' > ../../jsonfiles/funderdata/project.json
psql funderdata -f docOtherId2json.sql      | sed 's/\\\\/\\/g' > ../../jsonfiles/funderdata/docotherid.json
psql funderdata -f projDocOtherIds2json.sql | sed 's/\\\\/\\/g' > ../../jsonfiles/funderdata/projectdocotherid.json

echo "Done."
echo
