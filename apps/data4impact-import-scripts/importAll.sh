#!/bin/bash

java -jar /Users/michele/.m2/repository/eu/dnetlib/data4impact-importer/1.1.0-SNAPSHOT/data4impact-importer-1.1.0-SNAPSHOT.jar \
	./jsonfiles/swedishProjects/project.json \
	./jsonfiles/swedishProjects/projectOtherId.json \
	./jsonfiles/swedishProjects/organization.json \
	./jsonfiles/swedishProjects/projectOrganization.json \
	./jsonfiles/swedishProjects/docotherid.json \
	./jsonfiles/swedishProjects/projectdocotherid.json \
	./jsonfiles/ecProjectsOpenaire/project.json \
	./jsonfiles/ecProjectsOpenaire/projectOtherId.json \
	./jsonfiles/ecProjectsOpenaire/organization.json \
	./jsonfiles/ecProjectsOpenaire/organizationOtherId.json \
	./jsonfiles/ecProjectsOpenaire/projectOrganization.json \
	./jsonfiles/cordis/project.json \
	./jsonfiles/cordis/projectOtherId.json \
	./jsonfiles/cordis/organization.json \
	./jsonfiles/cordis/organizationOtherId.json \
	./jsonfiles/cordis/projectOrganization.json \
	./jsonfiles/companydata/orgCompanyMetrics.json \
	./jsonfiles/funderdata/project.json \
	./jsonfiles/funderdata/docotherid.json \
	./jsonfiles/funderdata/projectdocotherid.json \
	./jsonfiles/patents/document.json \
	./jsonfiles/patents/doc_fulltext.json \
	./jsonfiles/patents/doc_other_identifier.json \
	./jsonfiles/guidelines/document.json \
	./jsonfiles/guidelines/docotherid.json
	