package eu.data4impact.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.data4impact.repository.readonly.OrganizationViewRepository;
import eu.data4impact.utils.ReadOnlyRepository;
import eu.data4impact.views.OrganizationView;

@RestController
@RequestMapping("/api/organizations")
public class OrganizationController extends AbstractReadOnlyController<OrganizationView> {

	@Autowired
	private OrganizationViewRepository organizationViewRepository;

	@Override
	public ReadOnlyRepository<OrganizationView, String> getRepo() {
		return organizationViewRepository;
	}

	@RequestMapping(value = "/companies/{page}/{size}", method = RequestMethod.GET)
	public List<OrganizationView> findCompanies(@PathVariable final int page, @PathVariable final int size) {
		return organizationViewRepository.findByCompany(true, PageRequest.of(page, size)).getContent();
	}

	@RequestMapping(value = "/summary", method = RequestMethod.GET)
	public Map<String, Long> summary() {
		final Map<String, Long> res = new LinkedHashMap<>();
		res.put("all", organizationViewRepository.count());
		res.put("companies", organizationViewRepository.countByCompany(true));
		return res;
	}

}
