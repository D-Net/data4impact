package eu.data4impact.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.data4impact.model.topics.Topic;
import eu.data4impact.repository.TopicRepository;

@RestController
@RequestMapping("/api/topics")
public class TopicController extends AbstractJpaController<Topic> {

	@Autowired
	private TopicRepository topicRepository;

	@Override
	public JpaRepository<Topic, String> getRepo() {
		return topicRepository;
	}
}
