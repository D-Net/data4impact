package eu.data4impact.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.data4impact.utils.MainEntity;

public abstract class AbstractJpaController<T extends MainEntity> {

	public abstract JpaRepository<T, String> getRepo();

	@RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.GET)
	public final List<T> find(@PathVariable final int page, @PathVariable final int size) {
		return getRepo().findAll(PageRequest.of(page, size)).getContent();
	}

	@RequestMapping(value = "/identifiers/{page}/{size}", method = RequestMethod.GET)
	public final List<String> findIdentifiers(@PathVariable final int page, @PathVariable final int size) {
		return find(page, size).stream().map(MainEntity::getId).collect(Collectors.toList());
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public final T get(@RequestParam final String id) {
		return getRepo().findById(id).orElse(null);
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public final long count() {
		return getRepo().count();
	}
}
