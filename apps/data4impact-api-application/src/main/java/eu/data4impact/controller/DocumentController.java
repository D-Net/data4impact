package eu.data4impact.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.data4impact.model.documents.DocFulltext;
import eu.data4impact.model.documents.Document;
import eu.data4impact.repository.DocFulltextRepository;
import eu.data4impact.repository.DocumentRepository;

@RestController
@RequestMapping("/api/docs")
public class DocumentController extends AbstractJpaController<Document> {

	@Autowired
	private DocumentRepository documentRepository;

	@Autowired
	private DocFulltextRepository docFulltextRepository;

	@Override
	public JpaRepository<Document, String> getRepo() {
		return documentRepository;
	}

	@RequestMapping(value = "/fulltext", method = RequestMethod.GET, produces = "text/plain")
	public String fulltext(@RequestParam final String id) {
		return docFulltextRepository.findById(id).map(DocFulltext::getFulltext).orElse("");
	}

	@RequestMapping(value = "/byType/{type}/{page}/{size}", method = RequestMethod.GET)
	public List<Document> findByType(@PathVariable final String type, @PathVariable final int page, @PathVariable final int size) {
		return documentRepository.findByType(type, PageRequest.of(page, size)).getContent();
	}

	@RequestMapping(value = "/types", method = RequestMethod.GET)
	public Map<String, Long> types() {
		return documentRepository.types().stream().collect(Collectors.toMap(s -> s, s -> documentRepository.countByType(s)));
	}

}
