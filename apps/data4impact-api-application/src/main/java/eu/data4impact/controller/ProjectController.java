package eu.data4impact.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.postgresql.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.data4impact.model.projects.ProjectPortfolio;
import eu.data4impact.repository.ProjectPortfolioRepository;
import eu.data4impact.repository.readonly.ProjectViewRepository;
import eu.data4impact.utils.ReadOnlyRepository;
import eu.data4impact.views.ProjectView;

@RestController
@RequestMapping("/api/projects")
public class ProjectController extends AbstractReadOnlyController<ProjectView> {

	@Autowired
	private ProjectViewRepository projectRepository;

	@Autowired
	private ProjectPortfolioRepository projectPortfolioRepository;

	@Override
	public ReadOnlyRepository<ProjectView, String> getRepo() {
		return projectRepository;
	}

	@RequestMapping(value = "/byFunder/{funder}/{page}/{size}", method = RequestMethod.GET)
	public List<ProjectView> findByFunder(@PathVariable final String funder, @PathVariable final int page, @PathVariable final int size) {
		return projectRepository.findByFunder(funder, PageRequest.of(page, size)).getContent();
	}

	@RequestMapping(value = "/funders", method = RequestMethod.GET)
	public Map<String, Long> funders() {
		return projectRepository.funders().stream().collect(Collectors.toMap(s -> s, s -> projectRepository.countByFunder(s)));
	}

	@RequestMapping(value = "/portfolio", method = RequestMethod.GET)
	public final void getPortfolio(@RequestParam final String id, final HttpServletResponse res) throws IOException {
		res.setContentType("application/json");

		IOUtils.write(projectPortfolioRepository.findById(id)
				.map(ProjectPortfolio::getPortfolio)
				.map(Base64::decode)
				.map(this::gunzip)
				.orElse("{}"),
				res.getOutputStream(),
				Charset.defaultCharset());
	}

	private String gunzip(final byte[] bytes) {
		try (final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
				final GZIPInputStream gis = new GZIPInputStream(bis)) {
			return IOUtils.toString(gis, Charset.defaultCharset());
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

}
