package eu.data4impact.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.data4impact.utils.Counter;
import eu.data4impact.utils.DatabaseUtils;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private DatabaseUtils databaseUtils;

	@RequestMapping(value = "/materializedViews", method = RequestMethod.GET)
	public List<String> materializedViews(@RequestParam(required = false, defaultValue = "false") final boolean refresh) {
		return refresh ? databaseUtils.refreshMaterializedViews() : databaseUtils.materializedViews();
	}

	@Cacheable(value = "simpleCache", key = "'tables'")
	@RequestMapping(value = "/tables", method = RequestMethod.GET)
	public List<Counter> tables() {
		return databaseUtils.tableSizes();
	}

	@CacheEvict(cacheNames = { "simpleCache" }, allEntries = true)
	@RequestMapping(value = "/clearCaches", method = RequestMethod.GET)
	public List<String> clearCaches() {
		return Arrays.asList("Done.");
	}
}
