package eu.data4impact.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.data4impact.model.journals.Journal;
import eu.data4impact.repository.JournalRepository;

@RestController
@RequestMapping("/api/journals")
public class JournalController extends AbstractJpaController<Journal> {

	@Autowired
	private JournalRepository journalRepository;

	@Override
	public JpaRepository<Journal, String> getRepo() {
		return journalRepository;
	}

}
