package eu.data4impact;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import eu.data4impact.utils.DatabaseUtils;

@SpringBootApplication
public class PubmedImporterApplication implements CommandLineRunner {

	// private static final Logger log = LoggerFactory.getLogger(Data4ImpactImporterApplication.class);

	@Autowired
	private PubmedImporter importer;

	@Autowired
	private DatabaseUtils databaseUtils;

	public static void main(final String... args) {
		SpringApplication.run(PubmedImporterApplication.class, args);
	}

	@Override
	public void run(final String... args) throws IOException, InterruptedException {

		if (args.length != 2) {
			printHelp();
			System.exit(1);
		}

		final int nThreads = NumberUtils.toInt(args[0], 0);
		if (nThreads < 1) {
			System.out.println();
			System.err.println("Invalid number of threads: " + args[0]);
			printHelp();
			System.exit(1);
		}

		final Path dir = Paths.get(args[1]);

		if (!Files.exists(dir) || !Files.isDirectory(dir)) {
			System.out.println();
			System.err.println("Invalid directory: " + args[1]);
			printHelp();
			System.exit(1);
		}

		final List<Path> files = Files.list(dir)
				.filter(Files::isRegularFile)
				.filter(f -> f.toString().endsWith(".json"))
				.sorted((f1, f2) -> f1.getFileName().toString().compareTo(f2.getFileName().toString()))
				.collect(Collectors.toList());

		insertJournalAndProjects(files);
		insertDocuments(nThreads, files);

		System.out.println("Refreshing views...");
		databaseUtils.refreshMaterializedViews(v -> System.out.println(" - " + v));
		System.out.println("Done.\n");
	}

	private void insertJournalAndProjects(final List<Path> files) throws InterruptedException {
		final ExecutorService executor = Executors.newSingleThreadExecutor();
		final List<Callable<Long>> callables = new ArrayList<>();

		for (int i = 0; i < files.size(); i++) {
			callables.add(new PubmedImporterCallable(files.get(i), importer, i, files.size(), importer::processJournalsAndProjects, false));
		}

		final List<Future<Long>> futures = executor.invokeAll(callables);

		final Long total = futures.stream().map(f -> {
			try {
				return f.get();
			} catch (final Exception e) {
				throw new RuntimeException(e);
			}
		}).collect(Collectors.summingLong(n -> n));

		System.out.println("Number of inserted journals, projects and other identifiers, total = " + total);
	}

	private void insertDocuments(final int nThreads, final List<Path> files) throws InterruptedException {
		final ExecutorService executor = Executors.newFixedThreadPool(nThreads);

		final List<Callable<Long>> callables = new ArrayList<>();
		for (int i = 0; i < files.size(); i++) {
			callables.add(new PubmedImporterCallable(files.get(i), importer, i, files.size(), importer::processDocument, true));
		}

		final List<Future<Long>> futures = executor.invokeAll(callables);
		final Long total = futures.stream().map(f -> {
			try {
				return f.get();
			} catch (final Throwable e) {
				e.printStackTrace();
				return 0L;
			}
		}).collect(Collectors.summingLong(n -> n));

		System.out.println("Number of inserted entries, total = " + total);
	}

	private void printHelp() {
		System.out.println();
		System.out.println("Usage: [java application] [N_THREADS] [JSON_DIR]");
		System.out.println();
	}

}
