package eu.data4impact;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Callable;
import java.util.function.Function;

public class PubmedImporterCallable implements Callable<Long> {

	private final Path file;
	private final PubmedImporter importer;
	private final int current;
	private final int total;
	private final Function<String, Long> function;
	private final boolean renameFiles;

	public PubmedImporterCallable(final Path file, final PubmedImporter importer, final int current, final int total, final Function<String, Long> function,
			final boolean renameFiles) {
		this.file = file;
		this.importer = importer;
		this.current = current;
		this.total = total;
		this.function = function;
		this.renameFiles = renameFiles;
	}

	@Override
	public Long call() {
		try {
			final LocalDateTime start = LocalDateTime.now();
			final long count = importer.importPubmed(file, function);
			final LocalDateTime end = LocalDateTime.now();
			final double time = Duration.between(start, end).toNanos() / 1000000000.0;

			if (renameFiles) {
				Files.move(file, file.resolveSibling(file.getFileName() + ".done"));
			}

			System.out.printf("Done file %s (%d/%d) in %.3f sec (number of entries: %d)\n", file.getFileName(), current, total, time, count);

			return count;
		} catch (final Throwable e) {
			e.printStackTrace();
			return 0L;
		}
	}

}
