package eu.data4impact;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ObjectMerger {

	@SuppressWarnings("unchecked")
	public static <T> T mergeObjects(final T first, final T second) throws IllegalAccessException, InstantiationException {
		final Class<?> clazz = first.getClass();
		final Field[] fields = clazz.getDeclaredFields();
		final T res = (T) clazz.newInstance();
		for (final Field f : fields) {
			if (!Modifier.isFinal(f.getModifiers())) {
				f.setAccessible(true);
				final Object v1 = f.get(first);
				final Object v2 = f.get(second);
				f.set(res, (v2 != null ? v2 : v1));
			}
		}
		return res;
	}
}
