package eu.data4impact;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import eu.data4impact.model.documents.DocAuthor;
import eu.data4impact.model.documents.DocOtherId;
import eu.data4impact.model.documents.DocPdbCode;
import eu.data4impact.model.documents.DocSubject;
import eu.data4impact.model.documents.Document;
import eu.data4impact.model.journals.DocJournal;
import eu.data4impact.model.journals.Journal;
import eu.data4impact.model.journals.JournalOtherId;
import eu.data4impact.model.projects.DocProject;
import eu.data4impact.model.projects.Project;
import eu.data4impact.model.projects.ProjectOtherId;
import eu.data4impact.repository.DocAuthorRepository;
import eu.data4impact.repository.DocJournalRepository;
import eu.data4impact.repository.DocOtherIdRepository;
import eu.data4impact.repository.DocPdbCodeRepository;
import eu.data4impact.repository.DocProjectRepository;
import eu.data4impact.repository.DocSubjectRepository;
import eu.data4impact.repository.DocumentRepository;
import eu.data4impact.repository.JournalOtherIdRepository;
import eu.data4impact.repository.JournalRepository;
import eu.data4impact.repository.ProjectOtherIdRepository;
import eu.data4impact.repository.ProjectRepository;

@Component
public class PubmedImporter {


	public static final String WT_MOCK_IN = "wt__________::1e5e62235d094afd01cd56e65112fc63";
	public static final String WT_MOCK_OUT = "40|MOCK_PROJECT::f2869a8020d1cfb546b8497c7d0aa646";

	public static final String TARA_MOCK_IN = "taraexp_____::1e5e62235d094afd01cd56e65112fc63";
	public static final String TARA_MOCK_OUT = "40|MOCK_PROJECT::cc0229f5ebf339e024eac25aad00f5c1";

	private final ObjectMapper jsonMapper = new ObjectMapper();

	@Autowired
	private DocumentRepository documentRepository;
	@Autowired
	private JournalRepository journalRepository;
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private DocAuthorRepository docAuthorRepository;
	@Autowired
	private DocSubjectRepository docSubjectRepository;
	@Autowired
	private DocOtherIdRepository docOtherIdRepository;
	@Autowired
	private DocPdbCodeRepository docPdbCodeRepository;
	@Autowired
	private DocJournalRepository docJournalRepository;
	@Autowired
	private DocProjectRepository docProjectRepository;
	@Autowired
	private JournalOtherIdRepository journalOtherIdRepository;
	@Autowired
	private ProjectOtherIdRepository projectOtherIdRepository;

	private static HashSet<String> existingJournal = new HashSet<>();
	private static HashSet<String> existingProjects = new HashSet<>();

	@Transactional(TxType.REQUIRES_NEW)
	public long importPubmed(final Path file, final Function<String, Long> function) {
		try(Stream<String> lines = Files.lines(file, StandardCharsets.UTF_8)) {
            return lines
                    .map(this::extractBinary)
                    .map(this::decodeBase64)
                    .map(this::decompress)
                    .map(function)
                    .collect(Collectors.summingLong(n -> n));

		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	private String extractBinary(final String s) {
		try {
			return jsonMapper.readValue(s, ObjectNode.class).get("body").get("$binary").asText();
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	private byte[] decodeBase64(final String s) {
		return Base64.decodeBase64(s);
	}

	private String decompress(final byte[] input) {
		try (ByteArrayInputStream bis = new ByteArrayInputStream(input)) {
			try (ZipInputStream zis = new ZipInputStream(bis)) {
				ZipEntry ze;
				ze = zis.getNextEntry();
				if (ze == null) { throw new RuntimeException("cannot decompress null zip entry "); }
				if (!ze.getName().equals("body")) { throw new RuntimeException("cannot decompress zip entry name :" + ze.getName()); }
				return IOUtils.toString(zis, StandardCharsets.UTF_8);
			}
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public long processDocument(final String xml) {
		try {
			long count = 0;

			final SAXReader reader = new SAXReader();
			final org.dom4j.Document doc = reader.read(new StringReader(xml));

			final String docId = "50|" + doc.valueOf("/record/result/header/*[local-name()='objIdentifier']");
			final Node result = doc.selectSingleNode("/record/result/metadata/*[local-name()='entity']/*[local-name()='result']");
			final String pubyear = result.valueOf("./dateofacceptance").trim();
			// System.out.println(docId);

			final Document document = new Document();
			document.setId(docId);
			document.setTitle(result.valueOf("./title[@classid='main title']").trim());
			document.setAbstractText(result.valueOf("./description").trim());
			document.setType(result.valueOf("./resulttype/@classid"));
			document.setRights(result.valueOf("./bestaccessright/@classid"));
			document.setPubYear(pubyear);
			document.setRepository(result.valueOf("./collectedfrom/@name"));
			//document.setRepository("PubMed Central");
			document.setBatchid(StringUtils.substring(pubyear,0,5));
			documentRepository.save(document);
			count++;

			for (final Object o : result.selectNodes("./creator")) {
				final Node n = (Node) o;
				final DocAuthor author = new DocAuthor();
				author.setDocId(docId);
				author.setFullname(n.getText().trim());
				author.setRank(NumberUtils.toInt(n.valueOf("@rank"), 0));
				docAuthorRepository.save(author);
				count++;
			}

			for (final Object o : result.selectNodes("./subject")) {
				final Node n = (Node) o;
				final String term = n.getText().trim();
				final String type = n.valueOf("@classid").trim();
				if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(term)) {
					final DocSubject subject = new DocSubject();
					subject.setDocId(docId);
					subject.setTypology(type);
					subject.setSubject(term);
					docSubjectRepository.save(subject);
					count++;
				}
			}

			for (final Object o : result.selectNodes("./pid")) {
				final Node n = (Node) o;
				final String pid = n.getText().trim();
				if (StringUtils.isNotBlank(pid)) {
					final DocOtherId otherId = new DocOtherId();
					otherId.setDocId(docId);
					otherId.setId(pid);
					if (n.valueOf("@classid").equals("pmc")) {
						otherId.setType("pmcid");
					} else {
						otherId.setType(n.valueOf("@classid"));
					}
					docOtherIdRepository.save(otherId);
					count++;
				}
			}

			for (final Object o : result.selectNodes("./children/externalreference")) {
				final Node n = (Node) o;
				if (n.valueOf("./sitename").trim().equalsIgnoreCase("Protein Data Bank")) {
					final DocPdbCode code = new DocPdbCode();
					code.setDocId(docId);
					code.setPdbCode(n.valueOf("./refidentifier").trim());
					docPdbCodeRepository.save(code);
					count++;
				}
			}

			for (final Object o : result.selectNodes("./journal")) {
				final Node n = (Node) o;

				final String journalId;
				if (!n.valueOf("./@issn").isEmpty()) {
					journalId = "90|jrnl____issn::" + DigestUtils.md5Hex(n.valueOf("./@issn"));
				} else if (!n.valueOf("./@eissn").isEmpty()) {
					journalId = "90|jrnl___eissn::" + DigestUtils.md5Hex(n.valueOf("./@eissn"));
				} else if (!n.valueOf("./@lissn").isEmpty()) {
					journalId = "90|jrnl___lissn::" + DigestUtils.md5Hex(n.valueOf("./@lissn"));
				} else {
					journalId = "90|jrnl____name::" + DigestUtils.md5Hex(n.getText().trim());
				}

				final DocJournal docJournal = new DocJournal();
				docJournal.setDocId(docId);
				docJournal.setJournalId(journalId);
				docJournal.setInferred(false);
				docJournalRepository.save(docJournal);
				count++;
			}

			for (final Object o : result.selectNodes("./rels/rel")) {
				final Node n = (Node) o;

				if (n.valueOf("./to/@type").equals("project")) {
					final String to = n.valueOf("./to").trim();
					final DocProject docProject = new DocProject();
					docProject.setProjectId(getProjectId(to));
					docProject.setDocId(docId);
					docProject.setInferred(false);
					docProjectRepository.save(docProject);
					count++;
				}
			}
			return count;
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

    public long countPids(final String xml) {
        try {
            long count = 0;

            final SAXReader reader = new SAXReader();
            final org.dom4j.Document doc = reader.read(new StringReader(xml));

            final Node result = doc.selectSingleNode("/record/result/metadata/*[local-name()='entity']/*[local-name()='result']");

            for (final Object o : result.selectNodes("./pid")) {
                final Node n = (Node) o;
                final String pid = n.getText().trim();
                if (StringUtils.isNotBlank(pid)) {
                    if (n.valueOf("@classid").contains("pm")) {
                        count++;
                        break;
                    }
                }
            }

            return count;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }


    private String getProjectId(String to) {
		if (to.equals(WT_MOCK_IN)) {
			return WT_MOCK_OUT;
		} else if (to.equals(TARA_MOCK_IN)) {
			return TARA_MOCK_OUT;
		} else {
			return "40|" + to;
		}
	}

	public long processJournalsAndProjects(final String xml) {
		try {
			long count = 0;

			final SAXReader reader = new SAXReader();
			final org.dom4j.Document doc = reader.read(new StringReader(xml));
			final Node result = doc.selectSingleNode("/record/result/metadata/*[local-name()='entity']/*[local-name()='result']");

			for (final Object o : result.selectNodes("./journal")) {
				final Node n = (Node) o;

				final String journalId;
				if (!n.valueOf("./@issn").isEmpty()) {
					journalId = "90|jrnl____issn::" + DigestUtils.md5Hex(n.valueOf("./@issn"));
				} else if (!n.valueOf("./@eissn").isEmpty()) {
					journalId = "90|jrnl___eissn::" + DigestUtils.md5Hex(n.valueOf("./@eissn"));
				} else if (!n.valueOf("./@lissn").isEmpty()) {
					journalId = "90|jrnl___lissn::" + DigestUtils.md5Hex(n.valueOf("./@lissn"));
				} else {
					journalId = "90|jrnl____name::" + DigestUtils.md5Hex(n.getText().trim());
				}

				if (!existingJournal.contains(journalId)) {
					if (!journalRepository.existsById(journalId)) {

						final Journal journal = new Journal();
						journal.setId(journalId);
						journal.setTitle(n.getText().trim());
						journalRepository.save(journal);
						count++;

						if (!n.valueOf("./@issn").isEmpty()) {
							final JournalOtherId otherId = new JournalOtherId();
							otherId.setId(n.valueOf("./@issn"));
							otherId.setJournalId(journalId);
							otherId.setType("issn");
							journalOtherIdRepository.save(otherId);
							count++;
						}

						if (!n.valueOf("./@eissn").isEmpty()) {
							final JournalOtherId otherId = new JournalOtherId();
							otherId.setId(n.valueOf("./@eissn"));
							otherId.setJournalId(journalId);
							otherId.setType("eissn");
							journalOtherIdRepository.save(otherId);
							count++;
						}

						if (!n.valueOf("./@lissn").isEmpty()) {
							final JournalOtherId otherId = new JournalOtherId();
							otherId.setId(n.valueOf("./@lissn"));
							otherId.setJournalId(journalId);
							otherId.setType("lissn");
							journalOtherIdRepository.save(otherId);
							count++;
						}
					}
					existingJournal.add(journalId);
				}
			}

			for (final Object o : result.selectNodes("./rels/rel")) {
				final Node n = (Node) o;

				if (n.valueOf("./to/@type").equals("project")) {
					final String projectId = getProjectId(n.valueOf("./to").trim());
					if (!existingProjects.contains(projectId)) {
						if (!projectRepository.existsById(projectId)) {
							final Project project = new Project();
							project.setId(projectId);
							project.setTitle(n.valueOf("./title").trim());
							project.setAcronym(n.valueOf("./acronym").trim());
							String fundershortname = n.valueOf("./funding/funder/@shortname").trim();
							project.setFunder(getFunderName(fundershortname));
							project.setFundingLevel0(n.valueOf("./funding/funding_level_0/@name"));
							project.setFundingLevel1(n.valueOf("./funding/funding_level_1/@name"));
							project.setFundingLevel2(n.valueOf("./funding/funding_level_2/@name"));

							final String contractType = n.valueOf("./contracttype/@schemeid") + ":" + n.valueOf("./contracttype/@classid");
							if (contractType.length() > 1) {
								project.setContractType(contractType);
							}
							projectRepository.save(project);
							count++;

							if (!n.valueOf("./code").trim().isEmpty()) {
								final ProjectOtherId otherId = new ProjectOtherId();
								otherId.setId(n.valueOf("./code").trim());
								otherId.setProjectId(projectId);
								otherId.setType(fundershortname.toLowerCase() + ":grant_id");
								projectOtherIdRepository.save(otherId);
								count++;
							}
						}
						existingProjects.add(projectId);
					}

				}
			}

			return count;
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}

	}

	private String getFunderName(String fundershortname) {
		if ("WT".equals(fundershortname)) {
			return "Wellcome Trust";
		} else if ("FWF".equals(fundershortname)) {
			return "Austrian Science Fund FWF";
		} else if ("SNSF".equals(fundershortname)) {
			return "Swiss National Science Foundation SNSF";
		} else {
			return fundershortname;
		}
	}

}
