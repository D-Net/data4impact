package eu.data4impact;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import eu.data4impact.model.projects.Project;

public class ObjectMergerTest {

	@Test
	public void test() throws IllegalAccessException, InstantiationException {

		final Project p1 = new Project();
		final Project p2 = new Project();
		p2.setEcSc39(true);

		final Project p3 = ObjectMerger.mergeObjects(p1, p2);
		final Project p4 = ObjectMerger.mergeObjects(p2, p1);

		assertTrue(p3.getEcSc39());
		assertTrue(p4.getEcSc39());

	}

}
