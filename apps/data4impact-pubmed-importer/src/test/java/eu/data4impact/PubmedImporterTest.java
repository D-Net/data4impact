package eu.data4impact;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class PubmedImporterTest {

	private PubmedImporter importer;

	@Before
	public void setUp() {
		importer = new PubmedImporter();
	}

	@Test
	@Ignore
	public void testParse() {
		final Path path = Paths.get(getClass().getResource("/examples/record01.json").getFile());
		importer.importPubmed(path, importer::processDocument);
	}

}
