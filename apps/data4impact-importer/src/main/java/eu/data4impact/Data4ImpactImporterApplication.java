package eu.data4impact;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Table;

import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import eu.data4impact.utils.DatabaseUtils;

@SpringBootApplication
public class Data4ImpactImporterApplication implements CommandLineRunner {

	// private static final Logger log = LoggerFactory.getLogger(Data4ImpactImporterApplication.class);

	@Autowired
	private Data4ImpactImporter importer;

	@Autowired
	private DatabaseUtils databaseUtils;

	public static void main(final String... args) {
		SpringApplication.run(Data4ImpactImporterApplication.class, args);
	}

	@Override
	public void run(final String... args) {

		final Map<String, Class<?>> validEntities = validEntities();

		if (args.length == 0) {
			printHelp();
			printValidFiles(validEntities);
			System.exit(1);
		}

		for (final String f : args) {
			if (f.toLowerCase().endsWith(".json")) {
				System.out.println("Processing file: " + f);
				final Path path = Paths.get(f);
				final String fileName = path.getFileName().toString();
				final String entityName = fileName.substring(0, fileName.lastIndexOf('.')).toLowerCase();
				if (validEntities.containsKey(entityName)) {
					importer.importFileJson(path, validEntities.get(entityName));
				} else {
					System.err.println("\n[ERROR] Entity not found for file " + f);
					printValidFiles(validEntities);
					System.exit(-1);
				}
			} else {
				System.err.println("\nNot a json file: " + f);
				printValidFiles(validEntities);
				System.exit(-1);
			}
		}
		System.out.println("Refreshing views...");
		databaseUtils.refreshMaterializedViews(v -> System.out.println(" - " + v));
		System.out.println("Done.\n");
	}

	private void printHelp() {
		System.out.println();
		System.out.println("Missing input files !");
		System.out.println();
		System.out.println("Example: java -jar file1.json file2.json ...");
		System.out.println();
	}

	private void printValidFiles(final Map<String, Class<?>> validEntities) {
		System.out.println("\nValid filenames are (ignore case):\n" +
				validEntities.keySet()
						.stream()
						.collect(Collectors.groupingBy(validEntities::get))
						.entrySet()
						.stream()
						.map(e -> String.format(" - For class %s: %s\n",
								e.getKey().getSimpleName(),
								e.getValue().stream().collect(Collectors.joining(".json, ")) + ".json"))
						.collect(Collectors.joining()));
	}

	private Map<String, Class<?>> validEntities() {
		final Map<String, Class<?>> res = new HashMap<>();
		for (final Class<?> cl : new Reflections("eu.data4impact.model").getTypesAnnotatedWith(Table.class)) {
			res.put(cl.getSimpleName().toLowerCase(), cl);
			res.put(cl.getAnnotation(Table.class).name().toLowerCase(), cl);
		}
		return res;
	}

}
