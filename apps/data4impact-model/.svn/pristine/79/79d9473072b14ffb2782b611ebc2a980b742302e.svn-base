package eu.data4impact.model.topics;

import java.io.Serializable;
import java.util.Objects;

public class TopicPK implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3414377569275442254L;

    private Integer id;

    private String experimentId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(String experimentId) {
        this.experimentId = experimentId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, experimentId);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof TopicPK)) {
            return false;
        }
        final TopicPK other = (TopicPK) obj;
        return Objects.equals(id, other.id) && Objects.equals(experimentId, other.experimentId);
    }

    @Override
    public String toString() {
        return String.format("TopicPK [id=%s, experimentId=%s]", id, experimentId);
    }

}