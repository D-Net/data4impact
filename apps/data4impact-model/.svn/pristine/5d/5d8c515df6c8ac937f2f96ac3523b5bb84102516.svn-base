package eu.data4impact.model.documents;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the doc_subject database table.
 *
 */

public class DocSubjectPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2198475028032846904L;

	private String docId;

	private String subject;

	private String typology;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(final String typology) {
		this.typology = typology;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId, subject, typology);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof DocSubjectPK)) { return false; }
		final DocSubjectPK other = (DocSubjectPK) obj;
		return Objects.equals(docId, other.docId) && Objects.equals(subject, other.subject) && Objects.equals(typology, other.typology);
	}

	@Override
	public String toString() {
		return String.format("DocSubjectdPK [docId=%s, subject=%s, typology=%s]", docId, subject, typology);
	}

}
