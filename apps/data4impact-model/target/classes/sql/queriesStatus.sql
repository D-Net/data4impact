-- count the number of missing documents referred by the guidelines as "Matched references"
select idtype, count(*) from doc_other_identifier doi join doc_doc_other_id ddo on (doi.id=ddo.docid2) where docid is null and ddo.docid1 like '50|guideline%' and ddo.reltype = 'guidelines_matched' group by idtype ;
-- idtype | count
----------+-------
-- pmcid  |   511
-- pmid   |   500

-- count the number of missing documents referred by the guidelines as "All references"
select idtype, count(*) from doc_other_identifier doi join doc_doc_other_id ddo on (doi.id=ddo.docid2) where docid is null and ddo.docid1 like '50|guideline%' and ddo.reltype = 'guidelines_all' group by idtype ;
-- idtype | count
----------+--------
-- pmid   | 122276