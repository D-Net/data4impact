package eu.data4impact.views;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import eu.data4impact.utils.MainEntity;
import eu.data4impact.views.fields.RelatedEntity;
import eu.data4impact.views.fields.TypedIdentifier;

@Entity
@Table(name = "project_view")
@TypeDefs({
		@TypeDef(name = "json", typeClass = JsonStringType.class),
		@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class ProjectView implements Serializable, MainEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 7600811652343031045L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "title")
	private String title;

	@Column(name = "acronym")
	private String acronym;

	@Column(name = "callid")
	private String callId;

	@Column(name = "funder")
	private String funder;

	@Column(name = "fundinglevel0")
	private String fundingLevel0;

	@Column(name = "fundinglevel1")
	private String fundingLevel1;

	@Column(name = "fundinglevel2")
	private String fundingLevel2;

	@Column(name = "startdate")
	private String startDate;

	@Column(name = "enddate")
	private String endDate;

	@Column(name = "websiteurl")
	private String websiteUrl;

	@Column(name = "keywords")
	private String keywords;

	@Column(name = "contracttype")
	private String contractType;

	@Column(name = "ec_sc39")
	private Boolean ecSc39;

	@Column(name = "oa_mandate_for_publications")
	private Boolean oaMandateForPublications;

	@Column(name = "ec_article29_3")
	private Boolean ecArticle29_3;

	@Column(name = "abstract")
	private String abstractText;

	@Type(type = "json")
	@Column(name = "other_ids", columnDefinition = "json")
	private Set<TypedIdentifier> otherIdentifiers;

	@Type(type = "json")
	@Column(name = "participants", columnDefinition = "json")
	private Set<RelatedEntity> participants;

	@Override
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(final String callId) {
		this.callId = callId;
	}

	public String getFunder() {
		return funder;
	}

	public void setFunder(final String funder) {
		this.funder = funder;
	}

	public String getFundingLevel0() {
		return fundingLevel0;
	}

	public void setFundingLevel0(final String fundingLevel0) {
		this.fundingLevel0 = fundingLevel0;
	}

	public String getFundingLevel1() {
		return fundingLevel1;
	}

	public void setFundingLevel1(final String fundingLevel1) {
		this.fundingLevel1 = fundingLevel1;
	}

	public String getFundingLevel2() {
		return fundingLevel2;
	}

	public void setFundingLevel2(final String fundingLevel2) {
		this.fundingLevel2 = fundingLevel2;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(final String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(final String endDate) {
		this.endDate = endDate;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(final String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(final String keywords) {
		this.keywords = keywords;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(final String contractType) {
		this.contractType = contractType;
	}

	public Boolean getEcSc39() {
		return ecSc39;
	}

	public void setEcSc39(final Boolean ecSc39) {
		this.ecSc39 = ecSc39;
	}

	public Boolean getOaMandateForPublications() {
		return oaMandateForPublications;
	}

	public void setOaMandateForPublications(final Boolean oaMandateForPublications) {
		this.oaMandateForPublications = oaMandateForPublications;
	}

	public Boolean getEcArticle29_3() {
		return ecArticle29_3;
	}

	public void setEcArticle29_3(final Boolean ecArticle29_3) {
		this.ecArticle29_3 = ecArticle29_3;
	}

	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(final String abstractText) {
		this.abstractText = abstractText;
	}

	public Set<TypedIdentifier> getOtherIdentifiers() {
		return otherIdentifiers;
	}

	public void setOtherIdentifiers(final Set<TypedIdentifier> otherIdentifiers) {
		this.otherIdentifiers = otherIdentifiers;
	}

	public Set<RelatedEntity> getParticipants() {
		return participants;
	}

	public void setParticipants(final Set<RelatedEntity> participants) {
		this.participants = participants;
	}

}
