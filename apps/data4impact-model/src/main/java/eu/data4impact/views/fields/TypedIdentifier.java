package eu.data4impact.views.fields;

import java.io.Serializable;
import java.util.Objects;

public class TypedIdentifier implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5366779910072561204L;

	private String id;
	private String type;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof TypedIdentifier)) { return false; }
		final TypedIdentifier other = (TypedIdentifier) obj;
		return Objects.equals(id, other.id) && Objects.equals(type, other.type);
	}

}
