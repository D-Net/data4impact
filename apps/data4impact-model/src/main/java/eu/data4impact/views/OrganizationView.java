package eu.data4impact.views;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import eu.data4impact.utils.MainEntity;
import eu.data4impact.views.fields.RelatedEntity;
import eu.data4impact.views.fields.TypedIdentifier;

@Entity
@Table(name = "organization_view")
@TypeDefs({
		@TypeDef(name = "json", typeClass = JsonStringType.class),
		@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class OrganizationView implements Serializable, MainEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -8794901763526175534L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "shortname")
	private String shortName;

	@Column(name = "country")
	private String country;

	@Column(name = "street")
	private String street;

	@Column(name = "city")
	private String city;

	@Column(name = "postcode")
	private String postCode;

	@Column(name = "url")
	private String url;

	@Column(name = "ec_legalbody")
	private Boolean ecLegalBody;

	@Column(name = "ec_legalperson")
	private Boolean ecLegalPerson;

	@Column(name = "ec_nonprofit")
	private Boolean ecNonProfit;

	@Column(name = "ec_researchorganization")
	private Boolean ecResearchOrganization;

	@Column(name = "ec_highereducation")
	private Boolean ecHigherEducation;

	@Column(name = "ec_internationalorganizationeurinterests")
	private Boolean ecInternationalOrganizationEurInterests;

	@Column(name = "ec_internationalorganization")
	private Boolean ecInternationalOrganization;

	@Column(name = "ec_enterprise")
	private Boolean ecEnterprise;

	@Column(name = "ec_smevalidated")
	private Boolean ecSmeValidated;

	@Column(name = "ec_nutscode")
	private Boolean ecNutsCode;

	@Column(name = "company")
	private Boolean company;

	@Column(name = "data_gathered")
	private Boolean dataGathered;

	@Column(name = "tangible_pre_market")
	private Integer tangiblePreMarket;

	@Column(name = "tangible_market")
	private Integer tangibleMarket;

	@Column(name = "intangible_pre_market")
	private Integer intangiblePreMarket;

	@Column(name = "intangible_market")
	private Integer intangibleMarket;

	@Column(name = "innovation")
	private Boolean innovation;

	@Type(type = "json")
	@Column(name = "other_ids", columnDefinition = "json")
	private Set<TypedIdentifier> otherIdentifiers;

	@Type(type = "json")
	@Column(name = "projects", columnDefinition = "json")
	private Set<RelatedEntity> relatedProjects;

	@Override
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(final String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(final String postCode) {
		this.postCode = postCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public Boolean getEcLegalBody() {
		return ecLegalBody;
	}

	public void setEcLegalBody(final Boolean ecLegalBody) {
		this.ecLegalBody = ecLegalBody;
	}

	public Boolean getEcLegalPerson() {
		return ecLegalPerson;
	}

	public void setEcLegalPerson(final Boolean ecLegalPerson) {
		this.ecLegalPerson = ecLegalPerson;
	}

	public Boolean getEcNonProfit() {
		return ecNonProfit;
	}

	public void setEcNonProfit(final Boolean ecNonProfit) {
		this.ecNonProfit = ecNonProfit;
	}

	public Boolean getEcResearchOrganization() {
		return ecResearchOrganization;
	}

	public void setEcResearchOrganization(final Boolean ecResearchOrganization) {
		this.ecResearchOrganization = ecResearchOrganization;
	}

	public Boolean getEcHigherEducation() {
		return ecHigherEducation;
	}

	public void setEcHigherEducation(final Boolean ecHigherEducation) {
		this.ecHigherEducation = ecHigherEducation;
	}

	public Boolean getEcInternationalOrganizationEurInterests() {
		return ecInternationalOrganizationEurInterests;
	}

	public void setEcInternationalOrganizationEurInterests(final Boolean ecInternationalOrganizationEurInterests) {
		this.ecInternationalOrganizationEurInterests = ecInternationalOrganizationEurInterests;
	}

	public Boolean getEcInternationalOrganization() {
		return ecInternationalOrganization;
	}

	public void setEcInternationalOrganization(final Boolean ecInternationalOrganization) {
		this.ecInternationalOrganization = ecInternationalOrganization;
	}

	public Boolean getEcEnterprise() {
		return ecEnterprise;
	}

	public void setEcEnterprise(final Boolean ecEnterprise) {
		this.ecEnterprise = ecEnterprise;
	}

	public Boolean getEcSmeValidated() {
		return ecSmeValidated;
	}

	public void setEcSmeValidated(final Boolean ecSmeValidated) {
		this.ecSmeValidated = ecSmeValidated;
	}

	public Boolean getEcNutsCode() {
		return ecNutsCode;
	}

	public void setEcNutsCode(final Boolean ecNutsCode) {
		this.ecNutsCode = ecNutsCode;
	}

	public Boolean getCompany() {
		return company;
	}

	public void setCompany(final Boolean company) {
		this.company = company;
	}

	public Boolean getDataGathered() {
		return dataGathered;
	}

	public void setDataGathered(final Boolean dataGathered) {
		this.dataGathered = dataGathered;
	}

	public Integer getTangiblePreMarket() {
		return tangiblePreMarket;
	}

	public void setTangiblePreMarket(final Integer tangiblePreMarket) {
		this.tangiblePreMarket = tangiblePreMarket;
	}

	public Integer getTangibleMarket() {
		return tangibleMarket;
	}

	public void setTangibleMarket(final Integer tangibleMarket) {
		this.tangibleMarket = tangibleMarket;
	}

	public Integer getIntangiblePreMarket() {
		return intangiblePreMarket;
	}

	public void setIntangiblePreMarket(final Integer intangiblePreMarket) {
		this.intangiblePreMarket = intangiblePreMarket;
	}

	public Integer getIntangibleMarket() {
		return intangibleMarket;
	}

	public void setIntangibleMarket(final Integer intangibleMarket) {
		this.intangibleMarket = intangibleMarket;
	}

	public Boolean getInnovation() {
		return innovation;
	}

	public void setInnovation(final Boolean innovation) {
		this.innovation = innovation;
	}

	public Set<TypedIdentifier> getOtherIdentifiers() {
		return otherIdentifiers;
	}

	public void setOtherIdentifiers(final Set<TypedIdentifier> otherIdentifiers) {
		this.otherIdentifiers = otherIdentifiers;
	}

	public Set<RelatedEntity> getRelatedProjects() {
		return relatedProjects;
	}

	public void setRelatedProjects(final Set<RelatedEntity> relatedProjects) {
		this.relatedProjects = relatedProjects;
	}

}
