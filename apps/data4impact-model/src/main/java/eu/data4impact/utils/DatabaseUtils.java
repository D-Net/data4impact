package eu.data4impact.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class DatabaseUtils {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<String> refreshMaterializedViews() {
		final List<String> list = materializedViews();
		for (final String view : list) {
			jdbcTemplate.execute("REFRESH MATERIALIZED VIEW " + view);
		}
		return list;
	}

	public List<String> materializedViews() {
		return jdbcTemplate.queryForList("SELECT relname FROM pg_class WHERE relkind = 'm'", String.class);
	}

	public void refreshMaterializedViews(final Consumer<String> consumer) {
		for (final String view : materializedViews()) {
			consumer.accept(view);
			jdbcTemplate.execute("REFRESH MATERIALIZED VIEW " + view);
		}
	}

	public List<Counter> tableSizes() {
		try {
			final String sql = IOUtils.toString(getClass().getResourceAsStream("/sql/countAllTables.sql"), Charset.defaultCharset());
			return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(Counter.class));
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}

	}

}
