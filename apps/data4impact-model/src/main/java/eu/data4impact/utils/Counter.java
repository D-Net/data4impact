package eu.data4impact.utils;

import java.io.Serializable;

public class Counter implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 433019384222246549L;

	private String name;

	private Long count;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(final Long count) {
		this.count = count;
	}

}
