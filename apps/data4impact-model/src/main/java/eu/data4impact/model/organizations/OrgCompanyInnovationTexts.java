package eu.data4impact.model.organizations;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the org_company_innovation_texts database table.
 *
 */
@Entity
@Table(name = "org_company_innovation_texts")
@IdClass(OrgCompanyInnovationTextsPK.class)
public class OrgCompanyInnovationTexts implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5183231347005675116L;

	@Id
	@Column(name = "orgid")
	private String orgId;

	@Id
	@Column(name = "site_url")
	private String siteUrl;

	@Column(name = "prediction_revised")
	private Double predictionRevised;

	@Column(name = "source")
	private String source;

	@Column(name = "text_clean_gentle")
	private String textCleanGentle;

	@Column(name = "text_clean_strong")
	private String textCleanStrong;

	@Column(name = "text_is_duplicated")
	private Boolean duplicated;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(final String siteUrl) {
		this.siteUrl = siteUrl;
	}

	public Double getPredictionRevised() {
		return predictionRevised;
	}

	public void setPredictionRevised(final Double predictionRevised) {
		this.predictionRevised = predictionRevised;
	}

	public String getSource() {
		return source;
	}

	public void setSource(final String source) {
		this.source = source;
	}

	public String getTextCleanGentle() {
		return textCleanGentle;
	}

	public void setTextCleanGentle(final String textCleanGentle) {
		this.textCleanGentle = textCleanGentle;
	}

	public String getTextCleanStrong() {
		return textCleanStrong;
	}

	public void setTextCleanStrong(final String textCleanStrong) {
		this.textCleanStrong = textCleanStrong;
	}

	public Boolean getDuplicated() {
		return duplicated;
	}

	public void setDuplicated(final Boolean duplicated) {
		this.duplicated = duplicated;
	}

}
