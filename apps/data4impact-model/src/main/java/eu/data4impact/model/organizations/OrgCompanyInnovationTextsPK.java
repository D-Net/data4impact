package eu.data4impact.model.organizations;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the org_company_innovation_texts database table.
 *
 */
public class OrgCompanyInnovationTextsPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5738822465313162156L;

	private String orgId;

	private String siteUrl;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(final String siteUrl) {
		this.siteUrl = siteUrl;
	}

	@Override
	public int hashCode() {
		return Objects.hash(orgId, siteUrl);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OrgCompanyInnovationTextsPK)) { return false; }
		final OrgCompanyInnovationTextsPK other = (OrgCompanyInnovationTextsPK) obj;
		return Objects.equals(orgId, other.orgId) && Objects.equals(siteUrl, other.siteUrl);
	}

	@Override
	public String toString() {
		return String.format("OrgCompanyInnovationTextsPK [orgId=%s, siteUrl=%s]", orgId, siteUrl);
	}

}
