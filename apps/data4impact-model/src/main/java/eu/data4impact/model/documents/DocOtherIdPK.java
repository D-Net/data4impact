package eu.data4impact.model.documents;

import java.io.Serializable;
import java.util.Objects;

public class DocOtherIdPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8224314695851920139L;

	private String id;

	private String type;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof DocOtherIdPK)) { return false; }
		final DocOtherIdPK other = (DocOtherIdPK) obj;
		return Objects.equals(id, other.id) && Objects.equals(type, other.type);
	}

	@Override
	public String toString() {
		return String.format("DocOtherIdPK [id=%s, type=%s]", id, type);
	}

}
