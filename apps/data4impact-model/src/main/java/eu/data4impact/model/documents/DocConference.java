package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the doc_conference database table.
 *
 */
@Entity
@Table(name = "doc_conference")
public class DocConference implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6851805673136260456L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Column(name = "description")
	private String description;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

}
