package eu.data4impact.model.journals;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the doc_journal database table.
 *
 */
@Entity
@Table(name = "doc_journal")
@IdClass(DocJournalPK.class)
public class DocJournal implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5901850006873169898L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Id
	@Column(name = "journalid")
	private String journalId;

	@Column(name = "inferred", insertable = false, updatable = false)
	private Boolean inferred;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getJournalId() {
		return journalId;
	}

	public void setJournalId(final String journalId) {
		this.journalId = journalId;
	}

	public Boolean getInferred() {
		return inferred;
	}

	public void setInferred(final Boolean inferred) {
		this.inferred = inferred;
	}

}
