package eu.data4impact.model.organizations;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import eu.data4impact.utils.MainEntity;

/**
 * The persistent class for the organization database table.
 *
 */
@Entity
@Table(name = "organization")
public class Organization implements MainEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 9145717123500681168L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "shortname")
	private String shortName;

	@Column(name = "country")
	private String country;

	@Column(name = "street")
	private String street;

	@Column(name = "city")
	private String city;

	@Column(name = "postcode")
	private String postCode;

	@Column(name = "url")
	private String url;

	@Column(name = "ec_legalbody")
	private Boolean ecLegalBody;

	@Column(name = "ec_legalperson")
	private Boolean ecLegalPerson;

	@Column(name = "ec_nonprofit")
	private Boolean ecNonProfit;

	@Column(name = "ec_researchorganization")
	private Boolean ecResearchOrganization;

	@Column(name = "ec_highereducation")
	private Boolean ecHigherEducation;

	@Column(name = "ec_internationalorganizationeurinterests")
	private Boolean ecInternationalOrganizationEurInterests;

	@Column(name = "ec_internationalorganization")
	private Boolean ecInternationalOrganization;

	@Column(name = "ec_enterprise")
	private Boolean ecEnterprise;

	@Column(name = "ec_smevalidated")
	private Boolean ecSmeValidated;

	@Column(name = "ec_nutscode")
	private Boolean ecNutsCode;

	@Override
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(final String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(final String postCode) {
		this.postCode = postCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public Boolean isEcLegalBody() {
		return ecLegalBody;
	}

	public void setEcLegalBody(final Boolean ecLegalBody) {
		this.ecLegalBody = ecLegalBody;
	}

	public Boolean isEcLegalPerson() {
		return ecLegalPerson;
	}

	public void setEcLegalPerson(final Boolean ecLegalPerson) {
		this.ecLegalPerson = ecLegalPerson;
	}

	public Boolean isEcNonProfit() {
		return ecNonProfit;
	}

	public void setEcNonProfit(final Boolean ecNonProfit) {
		this.ecNonProfit = ecNonProfit;
	}

	public Boolean isEcResearchOrganization() {
		return ecResearchOrganization;
	}

	public void setEcResearchOrganization(final Boolean ecResearchOrganization) {
		this.ecResearchOrganization = ecResearchOrganization;
	}

	public Boolean isEcHigherEducation() {
		return ecHigherEducation;
	}

	public void setEcHigherEducation(final Boolean ecHigherEducation) {
		this.ecHigherEducation = ecHigherEducation;
	}

	public Boolean isEcInternationalOrganizationEurInterests() {
		return ecInternationalOrganizationEurInterests;
	}

	public void setEcInternationalOrganizationEurInterests(final Boolean ecInternationalOrganizationEurInterests) {
		this.ecInternationalOrganizationEurInterests = ecInternationalOrganizationEurInterests;
	}

	public Boolean isEcInternationalOrganization() {
		return ecInternationalOrganization;
	}

	public void setEcInternationalOrganization(final Boolean ecInternationalOrganization) {
		this.ecInternationalOrganization = ecInternationalOrganization;
	}

	public Boolean isEcEnterprise() {
		return ecEnterprise;
	}

	public void setEcEnterprise(final Boolean ecEnterprise) {
		this.ecEnterprise = ecEnterprise;
	}

	public Boolean isEcSmeValidated() {
		return ecSmeValidated;
	}

	public void setEcSmeValidated(final Boolean ecSmeValidated) {
		this.ecSmeValidated = ecSmeValidated;
	}

	public Boolean isEcNutsCode() {
		return ecNutsCode;
	}

	public void setEcNutsCode(final Boolean ecNutsCode) {
		this.ecNutsCode = ecNutsCode;
	}

}
