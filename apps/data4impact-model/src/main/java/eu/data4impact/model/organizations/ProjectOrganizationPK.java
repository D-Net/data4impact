package eu.data4impact.model.organizations;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the project_organization database table.
 *
 */
public class ProjectOrganizationPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6174643588649328556L;

	private String projectId;
	private String orgId;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(orgId, projectId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof ProjectOrganizationPK)) { return false; }
		final ProjectOrganizationPK other = (ProjectOrganizationPK) obj;
		return Objects.equals(orgId, other.orgId) && Objects.equals(projectId, other.projectId);
	}

	@Override
	public String toString() {
		return String.format("ProjectOrganizationPK [projectId=%s, orgId=%s]", projectId, orgId);
	}

}
