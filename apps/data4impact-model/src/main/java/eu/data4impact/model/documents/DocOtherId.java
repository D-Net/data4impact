package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "doc_other_identifier")
@IdClass(DocOtherIdPK.class)
public class DocOtherId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1422792251094308337L;

	@Id
	@Column(name = "id")
	private String id;

	@Id
	@Column(name = "idtype")
	private String type;

	@Column(name = "docid")
	private String docId;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}
}
