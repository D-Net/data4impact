package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the citation_with_other_identifier database table.
 *
 */
@Entity
@Table(name = "citation_other_identifier")
@IdClass(CitationOtherIdPK.class)
public class CitationOtherId implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1891544441955780413L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Id
	@Column(name = "citedid")
	private String citedId;

	@Id
	@Column(name = "citedidtype")
	private String citedIdType;

	@Column(name = "position")
	private Integer position;

	@Column(name = "citation")
	private String citation;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getCitedId() {
		return citedId;
	}

	public void setCitedId(final String citedId) {
		this.citedId = citedId;
	}

	public String getCitedIdType() {
		return citedIdType;
	}

	public void setCitedIdType(final String citedIdType) {
		this.citedIdType = citedIdType;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(final Integer position) {
		this.position = position;
	}

	public String getCitation() {
		return citation;
	}

	public void setCitation(final String citation) {
		this.citation = citation;
	}

}
