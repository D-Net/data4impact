package eu.data4impact.model.documents;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the citation database table.
 *
 */
public class CitationPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -931408472515374749L;

	private String docId;

	private String citedId;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getCitedId() {
		return citedId;
	}

	public void setCitedId(final String citedId) {
		this.citedId = citedId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(citedId, docId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof CitationPK)) { return false; }
		final CitationPK other = (CitationPK) obj;
		return Objects.equals(citedId, other.citedId) && Objects.equals(docId, other.docId);
	}

	@Override
	public String toString() {
		return String.format("CitationPK [docId=%s, citedId=%s]", docId, citedId);
	}

}
