package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import eu.data4impact.utils.MainEntity;

/**
 * The persistent class for the doclication database table.
 *
 */
@Entity
@Table(name = "document")
public class Document implements MainEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2687683721945336220L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "title")
	private String title;

	@Column(name = "abstract")
	private String abstractText;

	@Column(name = "doctype")
	private String type;

	@Column(name = "pubyear")
	private String pubYear;

	@Column(name = "repository")
	private String repository;

	@Column(name = "collection")
	private String collection;

	@Column(name = "rights")
	private String rights;

	@Column(name = "batchid")
	private String batchid;

	@Override
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(final String abstractText) {
		this.abstractText = abstractText;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getPubYear() {
		return pubYear;
	}

	public void setPubYear(final String pubYear) {
		this.pubYear = pubYear;
	}

	public String getRepository() {
		return repository;
	}

	public void setRepository(final String repository) {
		this.repository = repository;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(final String collection) {
		this.collection = collection;
	}

	public String getRights() {
		return rights;
	}

	public void setRights(final String rights) {
		this.rights = rights;
	}

	public String getBatchid() {
		return batchid;
	}

	public void setBatchid(final String batchid) {
		this.batchid = batchid;
	}
}
