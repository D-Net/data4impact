package eu.data4impact.model.organizations;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "organization_other_identifier")
@IdClass(OrganizationOtherIdPK.class)
public class OrganizationOtherId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1152547204961943043L;

	@Id
	@Column(name = "id")
	private String id;

	@Id
	@Column(name = "idtype")
	private String type;

	@Column(name = "orgid")
	private String orgId;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}
}
