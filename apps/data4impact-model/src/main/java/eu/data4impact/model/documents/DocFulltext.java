package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the doc_fulltext database table.
 *
 */
@Entity
@Table(name = "doc_fulltext")
public class DocFulltext implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4303769553390904582L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Column(name = "fulltext")
	private String fulltext;

	public DocFulltext() {}

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getFulltext() {
		return fulltext;
	}

	public void setFulltext(final String fulltext) {
		this.fulltext = fulltext;
	}

}
