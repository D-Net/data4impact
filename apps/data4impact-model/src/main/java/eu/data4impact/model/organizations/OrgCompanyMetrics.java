package eu.data4impact.model.organizations;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the org_company_metrics database table.
 *
 */
@Entity
@Table(name = "org_company_metrics")
public class OrgCompanyMetrics implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7211076285134438838L;

	@Id
	@Column(name = "orgid")
	private String orgId;

	@Column(name = "data_gathered")
	private Boolean dataGathered;

	@Column(name = "tangible_pre_market")
	private Integer tangiblePreMarket;

	@Column(name = "tangible_market")
	private Integer tangibleMarket;

	@Column(name = "intangible_pre_market")
	private Integer intangiblePreMarket;

	@Column(name = "intangible_market")
	private Integer intangibleMarket;

	@Column(name = "innovation")
	private Boolean innovation;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public Boolean isDataGathered() {
		return dataGathered;
	}

	public void setDataGathered(final Boolean dataGathered) {
		this.dataGathered = dataGathered;
	}

	public Integer getTangiblePreMarket() {
		return tangiblePreMarket;
	}

	public void setTangiblePreMarket(final Integer tangiblePreMarket) {
		this.tangiblePreMarket = tangiblePreMarket;
	}

	public Integer getTangibleMarket() {
		return tangibleMarket;
	}

	public void setTangibleMarket(final Integer tangibleMarket) {
		this.tangibleMarket = tangibleMarket;
	}

	public Integer getIntangiblePreMarket() {
		return intangiblePreMarket;
	}

	public void setIntangiblePreMarket(final Integer intangiblePreMarket) {
		this.intangiblePreMarket = intangiblePreMarket;
	}

	public Integer getIntangibleMarket() {
		return intangibleMarket;
	}

	public void setIntangibleMarket(final Integer intangibleMarket) {
		this.intangibleMarket = intangibleMarket;
	}

	public Boolean getInnovation() {
		return innovation;
	}

	public void setInnovation(final Boolean innovation) {
		this.innovation = innovation;
	}

}
