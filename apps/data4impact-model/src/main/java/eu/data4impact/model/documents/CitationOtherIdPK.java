package eu.data4impact.model.documents;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the citation_other_identifier database table.
 *
 */
public class CitationOtherIdPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8277865590023424127L;

	private String docId;

	private String citedId;

	private String citedIdType;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getCitedId() {
		return citedId;
	}

	public void setCitedId(final String citedId) {
		this.citedId = citedId;
	}

	public String getCitedIdType() {
		return citedIdType;
	}

	public void setCitedIdType(final String citedIdType) {
		this.citedIdType = citedIdType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(citedId, citedIdType, docId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof CitationOtherIdPK)) { return false; }
		final CitationOtherIdPK other = (CitationOtherIdPK) obj;
		return Objects.equals(citedId, other.citedId) && Objects.equals(citedIdType, other.citedIdType) && Objects.equals(docId, other.docId);
	}

	@Override
	public String toString() {
		return String.format("CitationWithOtherIdPK [docId=%s, citedId=%s, citedIdType=%s]", docId, citedId, citedIdType);
	}

}
