package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the doc_subject database table.
 *
 */
@Entity
@Table(name = "doc_subject")
@IdClass(DocSubjectPK.class)
public class DocSubject implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7863235934797184110L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Id
	@Column(name = "subject")
	private String subject;

	@Id
	@Column(name = "typology")
	private String typology;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(final String typology) {
		this.typology = typology;
	}

}
