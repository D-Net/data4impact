package eu.data4impact.model.projects;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "project_other_identifier")
@IdClass(ProjectOtherIdPK.class)
public class ProjectOtherId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9054182139882002670L;

	@Id
	@Column(name = "id")
	private String id;

	@Id
	@Column(name = "idtype")
	private String type;

	@Column(name = "projectid")
	private String projectId;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}
}
