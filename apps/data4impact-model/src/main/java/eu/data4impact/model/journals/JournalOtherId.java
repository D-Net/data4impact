package eu.data4impact.model.journals;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "journal_other_identifier")
@IdClass(JournalOtherIdPK.class)
public class JournalOtherId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3813641279381410579L;

	@Id
	@Column(name = "id")
	private String id;

	@Id
	@Column(name = "idtype")
	private String type;

	@Column(name = "journalid")
	private String journalId;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getJournalId() {
		return journalId;
	}

	public void setJournalId(final String journalId) {
		this.journalId = journalId;
	}
}
