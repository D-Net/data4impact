package eu.data4impact.model.topics;

import java.io.Serializable;

import javax.persistence.*;

import eu.data4impact.utils.MainEntity;

/**
 * The persistent class for the topic database table.
 *
 */
@Entity
@Table(name = "topic")
@IdClass(TopicPK.class)
public class Topic implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6445753926939700096L;

	@Id
	@Column(name = "id")
	private Integer id;

	@Id
	@Column(name = "experimentid")
	private String experimentId;

	@Column(name = "title")
	private String title;

	@Column(name = "category")
	private String category;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(final String category) {
		this.category = category;
	}

	public String getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}
}
