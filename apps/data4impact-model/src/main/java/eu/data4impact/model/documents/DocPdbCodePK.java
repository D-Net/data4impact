package eu.data4impact.model.documents;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the doc_pdbcode database table.
 *
 */
public class DocPdbCodePK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1821911037840095424L;

	private String docId;

	private String pdbCode;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getPdbCode() {
		return pdbCode;
	}

	public void setPdbCode(final String pdbCode) {
		this.pdbCode = pdbCode;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId, pdbCode);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof DocPdbCodePK)) { return false; }
		final DocPdbCodePK other = (DocPdbCodePK) obj;
		return Objects.equals(docId, other.docId) && Objects.equals(pdbCode, other.pdbCode);
	}

	@Override
	public String toString() {
		return String.format("DocPdbCodePK [docId=%s, pdbCode=%s]", docId, pdbCode);
	}

}
