package eu.data4impact.model.documents;

import java.io.Serializable;
import java.util.Objects;

public class DocDocumentPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1887148472112165819L;

	private String docId1;

	private String docId2;

	private String relType;

	public String getDocId1() {
		return docId1;
	}

	public void setDocId1(final String docId1) {
		this.docId1 = docId1;
	}

	public String getDocId2() {
		return docId2;
	}

	public void setDocId2(final String docId2) {
		this.docId2 = docId2;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId1, docId2, relType);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof DocDocumentPK)) { return false; }
		final DocDocumentPK other = (DocDocumentPK) obj;
		return Objects.equals(docId1, other.docId1) && Objects.equals(docId2, other.docId2) && Objects.equals(relType, other.relType);
	}

	@Override
	public String toString() {
		return String.format("DocDocumentPK [docId1=%s, docId2=%s, relType=%s]", docId1, docId2, relType);
	}

}
