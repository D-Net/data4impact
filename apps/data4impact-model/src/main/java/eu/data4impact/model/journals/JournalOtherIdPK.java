package eu.data4impact.model.journals;

import java.io.Serializable;
import java.util.Objects;

public class JournalOtherIdPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -723706788898427050L;

	private String id;

	private String type;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof JournalOtherIdPK)) { return false; }
		final JournalOtherIdPK other = (JournalOtherIdPK) obj;
		return Objects.equals(id, other.id) && Objects.equals(type, other.type);
	}

	@Override
	public String toString() {
		return String.format("JournalOtherIdPK [id=%s, type=%s]", id, type);
	}

}
