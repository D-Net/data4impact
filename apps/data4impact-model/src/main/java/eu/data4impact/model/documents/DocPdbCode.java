package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the doc_pdbcode database table.
 *
 */
@Entity
@Table(name = "doc_pdbcode")
@IdClass(DocPdbCodePK.class)
public class DocPdbCode implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8192602868047323880L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Id
	@Column(name = "pdbcode")
	private String pdbCode;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getPdbCode() {
		return pdbCode;
	}

	public void setPdbCode(final String pdbCode) {
		this.pdbCode = pdbCode;
	}

}
