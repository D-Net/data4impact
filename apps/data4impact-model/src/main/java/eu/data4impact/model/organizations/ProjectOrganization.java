package eu.data4impact.model.organizations;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the project_organization database table.
 *
 */
@Entity
@Table(name = "project_organization")
@IdClass(ProjectOrganizationPK.class)
public class ProjectOrganization implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -420774390787498160L;

	@Id
	@Column(name = "projectid")
	private String projectId;

	@Id
	@Column(name = "orgid")
	private String orgId;

	@Column(name = "role")
	private String role;

	@Column(name = "activitytype")
	private String activityType;

	@Column(name = "endofparticipation")
	private String endOfParticipation;

	@Column(name = "eccontribution")
	private String ecContribution;

	@Column(name = "contacttype")
	private String contactType;

	@Column(name = "contacttitle")
	private String contactTitle;

	@Column(name = "contactfirstnames")
	private String contactFirstNames;

	@Column(name = "contactlastnames")
	private String contactLastNames;

	@Column(name = "contactfunction")
	private String contactFunction;

	@Column(name = "contacttelephonenumber")
	private String contactTelephoneNumber;

	@Column(name = "contactfaxnumber")
	private String contactFaxNumber;

	@Column(name = "contactform")
	private String contactForm;

	@Column(name = "contribution")
	private BigDecimal contribution;

	@Column(name = "currency")
	private String currency;

	@Column(name = "inferred", insertable = false, updatable = false)
	private Boolean inferred;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(final String orgId) {
		this.orgId = orgId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(final String role) {
		this.role = role;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(final String activityType) {
		this.activityType = activityType;
	}

	public String getEndOfParticipation() {
		return endOfParticipation;
	}

	public void setEndOfParticipation(final String endOfParticipation) {
		this.endOfParticipation = endOfParticipation;
	}

	public String getEcContribution() {
		return ecContribution;
	}

	public void setEcContribution(final String ecContribution) {
		this.ecContribution = ecContribution;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(final String contactType) {
		this.contactType = contactType;
	}

	public String getContactTitle() {
		return contactTitle;
	}

	public void setContactTitle(final String contactTitle) {
		this.contactTitle = contactTitle;
	}

	public String getContactFirstNames() {
		return contactFirstNames;
	}

	public void setContactFirstNames(final String contactFirstNames) {
		this.contactFirstNames = contactFirstNames;
	}

	public String getContactLastNames() {
		return contactLastNames;
	}

	public void setContactLastNames(final String contactLastNames) {
		this.contactLastNames = contactLastNames;
	}

	public String getContactFunction() {
		return contactFunction;
	}

	public void setContactFunction(final String contactFunction) {
		this.contactFunction = contactFunction;
	}

	public String getContactTelephoneNumber() {
		return contactTelephoneNumber;
	}

	public void setContactTelephoneNumber(final String contactTelephoneNumber) {
		this.contactTelephoneNumber = contactTelephoneNumber;
	}

	public String getContactFaxNumber() {
		return contactFaxNumber;
	}

	public void setContactFaxNumber(final String contactFaxNumber) {
		this.contactFaxNumber = contactFaxNumber;
	}

	public String getContactForm() {
		return contactForm;
	}

	public void setContactForm(final String contactForm) {
		this.contactForm = contactForm;
	}

	public Boolean getInferred() {
		return inferred;
	}

	public void setInferred(final Boolean inferred) {
		this.inferred = inferred;
	}

	public BigDecimal getContribution() {
		return contribution;
	}

	public void setContribution(BigDecimal contribution) {
		this.contribution = contribution;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
