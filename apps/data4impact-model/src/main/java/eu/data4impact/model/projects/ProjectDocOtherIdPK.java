package eu.data4impact.model.projects;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the project_doc_other_id database table.
 *
 */
public class ProjectDocOtherIdPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5262165786311660789L;

	private String projectId;

	private String docId;

	private String docIdType;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getDocIdType() {
		return docIdType;
	}

	public void setDocIdType(final String docIdType) {
		this.docIdType = docIdType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId, docIdType, projectId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof ProjectDocOtherIdPK)) { return false; }
		final ProjectDocOtherIdPK other = (ProjectDocOtherIdPK) obj;
		return Objects.equals(docId, other.docId) && Objects.equals(docIdType, other.docIdType) && Objects.equals(projectId, other.projectId);
	}

	@Override
	public String toString() {
		return String.format("ProjectDocOtherIdPK [projectId=%s, docId=%s, docIdType=%s]", projectId, docId, docIdType);
	}

}
