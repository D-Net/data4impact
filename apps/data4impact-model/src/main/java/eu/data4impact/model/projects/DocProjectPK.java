package eu.data4impact.model.projects;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the doc_project database table.
 *
 */
public class DocProjectPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5677313653466737245L;

	private String projectId;

	private String docId;

	public DocProjectPK() {}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(projectId, docId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof DocProjectPK)) { return false; }
		final DocProjectPK other = (DocProjectPK) obj;
		return Objects.equals(projectId, other.projectId) && Objects.equals(docId, other.docId);
	}

	@Override
	public String toString() {
		return String.format("DocProjectPK [projectId=%s, docId=%s]", projectId, docId);
	}

}
