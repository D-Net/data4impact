package eu.data4impact.model.projects;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the project_doc_other_id database table.
 *
 */
@Entity
@Table(name = "project_doc_other_id")
@IdClass(ProjectDocOtherIdPK.class)
public class ProjectDocOtherId implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 9201756577322864825L;

	@Id
	@Column(name = "projectid")
	private String projectId;

	@Id
	@Column(name = "docid")
	private String docId;
	@Id
	@Column(name = "docidtype")
	private String docIdType;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getDocIdType() {
		return docIdType;
	}

	public void setDocIdType(final String docIdType) {
		this.docIdType = docIdType;
	}

}
