package eu.data4impact.model.topics;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the doc_topic database table.
 *
 */
@Entity
@Table(name = "doc_topic")
@IdClass(DocTopicPK.class)
public class DocTopic implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8086079104455989103L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Id
	@Column(name = "topicid")
	private Integer topicId;

	@Id
	@Column(name = "experimentid")
	private String experimentId;

	@Column(name = "inferred", insertable = false, updatable = false)
	private Boolean inferred;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public Integer getTopicId() {
		return topicId;
	}

	public void setTopicId(final Integer topicId) {
		this.topicId = topicId;
	}

	public Boolean getInferred() {
		return inferred;
	}

	public void setInferred(final Boolean inferred) {
		this.inferred = inferred;
	}

	public String getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}
}
