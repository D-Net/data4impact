package eu.data4impact.model.projects;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the project_portfolio database table.
 *
 */
@Entity
@Table(name = "project_portfolio")
public class ProjectPortfolio implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3357715505269080465L;

	@Id
	@Column(name = "projectid")
	private String projectId;

	@Column(name = "portfolio")
	private String portfolio;



	public String getProjectId() {
		return projectId;
	}

	public String getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(final String portfolio) {
		this.portfolio = portfolio;
	}

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}


}
