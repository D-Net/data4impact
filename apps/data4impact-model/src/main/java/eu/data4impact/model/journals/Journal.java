package eu.data4impact.model.journals;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import eu.data4impact.utils.MainEntity;

/**
 * The persistent class for the journal database table.
 *
 */
@Entity
@Table(name = "journal")
public class Journal implements MainEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8266965444918722849L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "title")
	private String title;

	public Journal() {}

	@Override
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

}
