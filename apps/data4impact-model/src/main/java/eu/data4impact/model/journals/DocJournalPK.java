package eu.data4impact.model.journals;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the doc_journal database table.
 *
 */
public class DocJournalPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 9027444769592530131L;

	private String docId;

	private String journalId;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getJournalId() {
		return journalId;
	}

	public void setJournalId(final String journalId) {
		this.journalId = journalId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId, journalId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof DocJournalPK)) { return false; }
		final DocJournalPK other = (DocJournalPK) obj;
		return Objects.equals(docId, other.docId) && Objects.equals(journalId, other.journalId);
	}

	@Override
	public String toString() {
		return String.format("DocJournalPK [docId=%s, journalId=%s]", docId, journalId);
	}

}
