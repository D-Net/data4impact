package eu.data4impact.model.projects;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import eu.data4impact.utils.MainEntity;

/**
 * The persistent class for the project database table.
 *
 */
@Entity
@Table(name = "project")
public class Project implements MainEntity, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4583515393576103315L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "title")
	private String title;

	@Column(name = "acronym")
	private String acronym;

	@Column(name = "callid")
	private String callId;

	@Column(name = "funder")
	private String funder;

	@Column(name = "fundinglevel0")
	private String fundingLevel0;

	@Column(name = "fundinglevel1")
	private String fundingLevel1;

	@Column(name = "fundinglevel2")
	private String fundingLevel2;

	@Column(name = "startdate")
	private String startDate;

	@Column(name = "enddate")
	private String endDate;

	@Column(name = "websiteurl")
	private String websiteUrl;

	@Column(name = "keywords")
	private String keywords;

	@Column(name = "contracttype")
	private String contractType;

	@Column(name = "ec_sc39")
	private Boolean ecSc39;

	@Column(name = "oa_mandate_for_publications")
	private Boolean oaMandateForPublications;

	@Column(name = "ec_article29_3")
	private Boolean ecArticle29_3;

	@Column(name = "abstract")
	private String abstractText;

	@Column(name = "contribution")
	private BigDecimal contribution;

	@Column(name = "total_cost")
	private BigDecimal totalCost;

	@Column(name = "currency")
	private String currency;

	@Override
	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(final String acronym) {
		this.acronym = acronym;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(final String callId) {
		this.callId = callId;
	}

	public String getFunder() {
		return funder;
	}

	public void setFunder(final String funder) {
		this.funder = funder;
	}

	public String getFundingLevel0() {
		return fundingLevel0;
	}

	public void setFundingLevel0(final String fundingLevel0) {
		this.fundingLevel0 = fundingLevel0;
	}

	public String getFundingLevel1() {
		return fundingLevel1;
	}

	public void setFundingLevel1(final String fundingLevel1) {
		this.fundingLevel1 = fundingLevel1;
	}

	public String getFundingLevel2() {
		return fundingLevel2;
	}

	public void setFundingLevel2(final String fundingLevel2) {
		this.fundingLevel2 = fundingLevel2;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(final String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(final String endDate) {
		this.endDate = endDate;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(final String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(final String keywords) {
		this.keywords = keywords;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(final String contractType) {
		this.contractType = contractType;
	}

	public Boolean getEcSc39() {
		return ecSc39;
	}

	public void setEcSc39(final Boolean ecSc39) {
		this.ecSc39 = ecSc39;
	}

	public Boolean getOaMandateForPublications() {
		return oaMandateForPublications;
	}

	public void setOaMandateForPublications(final Boolean oaMandateForPublications) {
		this.oaMandateForPublications = oaMandateForPublications;
	}

	public Boolean getEcArticle29_3() {
		return ecArticle29_3;
	}

	public void setEcArticle29_3(final Boolean ecArticle29_3) {
		this.ecArticle29_3 = ecArticle29_3;
	}

	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(final String abstractText) {
		this.abstractText = abstractText;
	}

	public BigDecimal getContribution() {
		return contribution;
	}

	public void setContribution(BigDecimal contribution) {
		this.contribution = contribution;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
