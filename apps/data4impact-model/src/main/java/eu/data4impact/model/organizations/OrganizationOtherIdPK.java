package eu.data4impact.model.organizations;

import java.io.Serializable;
import java.util.Objects;

public class OrganizationOtherIdPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6411887710166872003L;

	private String id;

	private String type;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof OrganizationOtherIdPK)) { return false; }
		final OrganizationOtherIdPK other = (OrganizationOtherIdPK) obj;
		return Objects.equals(id, other.id) && Objects.equals(type, other.type);
	}

	@Override
	public String toString() {
		return String.format("OrganizationOtherIdPK [id=%s, type=%s]", id, type);
	}

}
