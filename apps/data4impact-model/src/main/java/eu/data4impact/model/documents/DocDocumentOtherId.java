package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the doc_doc_other_id database table.
 *
 */
@Entity
@Table(name = "doc_doc_other_id")
@IdClass(DocDocumentOtherIdPK.class)
public class DocDocumentOtherId implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2328915939893536288L;

	@Id
	@Column(name = "docid1")
	private String docId1;

	@Id
	@Column(name = "docid2")
	private String docId2;

	@Id
	@Column(name = "docid2type")
	private String docId2Type;

	@Id
	@Column(name = "reltype")
	private String relType;

	public String getDocId1() {
		return docId1;
	}

	public void setDocId1(final String docId1) {
		this.docId1 = docId1;
	}

	public String getDocId2() {
		return docId2;
	}

	public void setDocId2(final String docId2) {
		this.docId2 = docId2;
	}

	public String getDocId2Type() {
		return docId2Type;
	}

	public void setDocId2Type(final String docId2Type) {
		this.docId2Type = docId2Type;
	}

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

}
