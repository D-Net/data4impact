package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the citation database table.
 *
 */
@Entity
@Table(name = "citation")
@IdClass(CitationPK.class)
public class Citation implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2192987255711687444L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Id
	@Column(name = "citedid")
	private String citedId;

	@Column(name = "position")
	private Integer position;

	@Column(name = "citation")
	private String citation;

	@Column(name = "inferred", insertable = false, updatable = false)
	private Boolean inferred;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getCitedId() {
		return citedId;
	}

	public void setCitedId(final String citedId) {
		this.citedId = citedId;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(final Integer position) {
		this.position = position;
	}

	public String getCitation() {
		return citation;
	}

	public void setCitation(final String citation) {
		this.citation = citation;
	}

	public Boolean getInferred() {
		return inferred;
	}

	public void setInferred(final Boolean inferred) {
		this.inferred = inferred;
	}

}
