package eu.data4impact.model.topics;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the doc_topic database table.
 *
 */
public class DocTopicPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3414377569275442254L;

	private String docId;

	private Integer topicId;

	private String experimentId;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public Integer getTopicId() {
		return topicId;
	}

	public void setTopicId(final Integer topicId) {
		this.topicId = topicId;
	}

	public String getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId, topicId, experimentId);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof DocTopicPK)) { return false; }
		final DocTopicPK other = (DocTopicPK) obj;
		return Objects.equals(docId, other.docId) && Objects.equals(topicId, other.topicId) && Objects.equals(experimentId, other.experimentId);
	}

	@Override
	public String toString() {
		return String.format("DocTopicPK [docId=%s, topicId=%s, experimentId=%s]", docId, topicId, experimentId);
	}


}
