package eu.data4impact.model.documents;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * The persistent class for the doc_author database table.
 *
 */
@Entity
@Table(name = "doc_author")
@IdClass(DocAuthorPK.class)
public class DocAuthor implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7063744014658539157L;

	@Id
	@Column(name = "docid")
	private String docId;

	@Id
	@Column(name = "fullname")
	private String fullname;

	@Column(name = "rank")
	private Integer rank;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(final String fullname) {
		this.fullname = fullname;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(final Integer rank) {
		this.rank = rank;
	}

}
