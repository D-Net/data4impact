package eu.data4impact.model.documents;

import java.io.Serializable;
import java.util.Objects;

/**
 * The primary key class for the doc_author database table.
 *
 */
public class DocAuthorPK implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -595729184225885345L;

	private String docId;

	private String fullname;

	public String getDocId() {
		return docId;
	}

	public void setDocId(final String docId) {
		this.docId = docId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(final String fullname) {
		this.fullname = fullname;
	}

	@Override
	public int hashCode() {
		return Objects.hash(docId, fullname);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof DocAuthorPK)) { return false; }
		final DocAuthorPK other = (DocAuthorPK) obj;
		return Objects.equals(docId, other.docId) && Objects.equals(fullname, other.fullname);
	}

	@Override
	public String toString() {
		return String.format("DocAuthorPK [docId=%s, fullname=%s]", docId, fullname);
	}

}
