package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.organizations.OrgCompanyMetrics;

@Repository
public interface OrgCompanyMetricsRepository extends JpaRepository<OrgCompanyMetrics, String> {}
