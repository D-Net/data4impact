package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.projects.DocProject;
import eu.data4impact.model.projects.DocProjectPK;

@Repository
public interface DocProjectRepository extends JpaRepository<DocProject, DocProjectPK> {}
