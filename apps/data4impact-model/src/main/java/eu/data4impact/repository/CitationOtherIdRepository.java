package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.CitationOtherId;
import eu.data4impact.model.documents.CitationOtherIdPK;

@Repository
public interface CitationOtherIdRepository extends JpaRepository<CitationOtherId, CitationOtherIdPK> {}
