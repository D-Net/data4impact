package eu.data4impact.repository.readonly;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import eu.data4impact.utils.ReadOnlyRepository;
import eu.data4impact.views.OrganizationView;

@Repository
public interface OrganizationViewRepository extends ReadOnlyRepository<OrganizationView, String> {

	Page<OrganizationView> findByCompany(boolean isCompany, Pageable pageable);

	long countByCompany(boolean isCompany);
}
