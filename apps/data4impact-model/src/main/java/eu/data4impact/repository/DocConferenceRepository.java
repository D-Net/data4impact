package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.DocConference;

@Repository
public interface DocConferenceRepository extends JpaRepository<DocConference, String> {}
