package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.organizations.OrgCompanyInnovationTexts;
import eu.data4impact.model.organizations.OrgCompanyInnovationTextsPK;

@Repository
public interface OrgCompanyInnovationTextsRepository extends JpaRepository<OrgCompanyInnovationTexts, OrgCompanyInnovationTextsPK> {}
