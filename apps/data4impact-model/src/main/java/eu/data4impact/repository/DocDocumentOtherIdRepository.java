package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.DocDocumentOtherId;
import eu.data4impact.model.documents.DocDocumentOtherIdPK;

@Repository
public interface DocDocumentOtherIdRepository extends JpaRepository<DocDocumentOtherId, DocDocumentOtherIdPK> {}
