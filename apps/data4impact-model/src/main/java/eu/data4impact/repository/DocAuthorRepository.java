package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.DocAuthor;
import eu.data4impact.model.documents.DocAuthorPK;

@Repository
public interface DocAuthorRepository extends JpaRepository<DocAuthor, DocAuthorPK> {}
