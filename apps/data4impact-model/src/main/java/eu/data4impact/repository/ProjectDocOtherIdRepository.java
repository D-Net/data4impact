package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.projects.ProjectDocOtherId;
import eu.data4impact.model.projects.ProjectDocOtherIdPK;

@Repository
public interface ProjectDocOtherIdRepository extends JpaRepository<ProjectDocOtherId, ProjectDocOtherIdPK> {}
