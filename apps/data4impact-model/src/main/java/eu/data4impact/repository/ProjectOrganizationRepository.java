package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.organizations.ProjectOrganization;
import eu.data4impact.model.organizations.ProjectOrganizationPK;

@Repository
public interface ProjectOrganizationRepository extends JpaRepository<ProjectOrganization, ProjectOrganizationPK> {}
