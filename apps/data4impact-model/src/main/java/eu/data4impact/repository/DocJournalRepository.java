package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.journals.DocJournal;
import eu.data4impact.model.journals.DocJournalPK;

@Repository
public interface DocJournalRepository extends JpaRepository<DocJournal, DocJournalPK> {}
