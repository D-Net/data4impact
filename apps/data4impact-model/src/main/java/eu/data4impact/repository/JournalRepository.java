package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.journals.Journal;

@Repository
public interface JournalRepository extends JpaRepository<Journal, String> {}
