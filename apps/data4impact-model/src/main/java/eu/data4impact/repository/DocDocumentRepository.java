package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.DocDocument;
import eu.data4impact.model.documents.DocDocumentPK;

@Repository
public interface DocDocumentRepository extends JpaRepository<DocDocument, DocDocumentPK> {}
