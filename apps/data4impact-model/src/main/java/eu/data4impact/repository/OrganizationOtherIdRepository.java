package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.organizations.OrganizationOtherId;
import eu.data4impact.model.organizations.OrganizationOtherIdPK;

@Repository
public interface OrganizationOtherIdRepository extends JpaRepository<OrganizationOtherId, OrganizationOtherIdPK> {}
