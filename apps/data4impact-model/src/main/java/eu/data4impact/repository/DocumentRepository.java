package eu.data4impact.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, String> {

	long countByType(String s);

	@Query(value = "select distinct doctype from document", nativeQuery = true)
	List<String> types();

	Page<Document> findByType(String type, Pageable pageable);
}
