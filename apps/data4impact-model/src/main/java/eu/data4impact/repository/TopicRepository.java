package eu.data4impact.repository;

import eu.data4impact.model.topics.TopicPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.topics.Topic;

@Repository
public interface TopicRepository extends JpaRepository<Topic, TopicPK> {}
