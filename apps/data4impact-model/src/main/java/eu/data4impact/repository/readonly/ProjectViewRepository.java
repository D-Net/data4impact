package eu.data4impact.repository.readonly;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.data4impact.utils.ReadOnlyRepository;
import eu.data4impact.views.ProjectView;

@Repository
public interface ProjectViewRepository extends ReadOnlyRepository<ProjectView, String> {

	Page<ProjectView> findByFunder(String funder, Pageable pageable);

	long countByFunder(String s);

	@Query(value = "select distinct funder from project", nativeQuery = true)
	List<String> funders();
}
