package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.projects.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {}
