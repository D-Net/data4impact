package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.Citation;
import eu.data4impact.model.documents.CitationPK;

@Repository
public interface CitationRepository extends JpaRepository<Citation, CitationPK> {}
