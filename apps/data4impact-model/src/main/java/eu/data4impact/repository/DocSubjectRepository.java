package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.DocSubject;
import eu.data4impact.model.documents.DocSubjectPK;

@Repository
public interface DocSubjectRepository extends JpaRepository<DocSubject, DocSubjectPK> {}
