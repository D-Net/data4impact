package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.documents.DocOtherId;
import eu.data4impact.model.documents.DocOtherIdPK;

@Repository
public interface DocOtherIdRepository extends JpaRepository<DocOtherId, DocOtherIdPK> {}
