package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.topics.DocTopic;
import eu.data4impact.model.topics.DocTopicPK;

@Repository
public interface DocTopicRepository extends JpaRepository<DocTopic, DocTopicPK> {}
