package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.projects.ProjectOtherId;
import eu.data4impact.model.projects.ProjectOtherIdPK;

@Repository
public interface ProjectOtherIdRepository extends JpaRepository<ProjectOtherId, ProjectOtherIdPK> {}
