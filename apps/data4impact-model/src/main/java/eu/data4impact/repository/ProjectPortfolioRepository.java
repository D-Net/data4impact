package eu.data4impact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import eu.data4impact.model.projects.ProjectPortfolio;

@Repository
public interface ProjectPortfolioRepository extends JpaRepository<ProjectPortfolio, String> {}
