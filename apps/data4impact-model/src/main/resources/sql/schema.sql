SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

-- MAIN ENTITIES --

CREATE TABLE document (
    id          text PRIMARY KEY,
    title       text,
    abstract    text,
    doctype     text,
    repository  text,
    collection  text,
    pubyear     text,
    rights      text,
    batchid     text
);

CREATE TABLE project (
    id            text PRIMARY KEY,
    title         text,
    acronym       text,
    funder        text,
    fundinglevel0 text,
    fundinglevel1 text,
    fundinglevel2 text,
    callid        text,
    startdate     text,
    enddate       text,
    websiteurl    text, 
	keywords      text, 
	contracttype  text,
	abstract      text,
	ec_sc39                     boolean,
	oa_mandate_for_publications boolean,
	ec_article29_3              boolean,
	total_cost      numeric,
	contribution    numeric,
	currency        text
);

CREATE TABLE journal (
    id         text PRIMARY KEY,
    title      text
);

CREATE TABLE organization (
	id                                       text PRIMARY KEY,
	name                                     text,
	shortname                                text,
	country                                  text,
	street                                   text,
	city                                     text,
	postcode                                 text,
	url                                      text,
	ec_legalbody                             boolean,
	ec_legalperson                           boolean,
	ec_nonprofit                             boolean,
	ec_researchorganization                  boolean,
	ec_highereducation                       boolean,
	ec_internationalorganizationeurinterests boolean,
	ec_internationalorganization             boolean,
	ec_enterprise                            boolean,
	ec_smevalidated                          boolean,
	ec_nutscode                              boolean
);

CREATE TABLE topic (
	id        integer,
    title     text,
    category  text,
    experimentid text COLLATE pg_catalog."default" NOT NULL,
    visibilityindex integer,
    comments text COLLATE pg_catalog."default",
    icd11 text COLLATE pg_catalog."default",
    CONSTRAINT topicdescription_pkey PRIMARY KEY (id, experimentid)
);



-- OTHER IDENTIFIERS TABLES --

CREATE TABLE doc_other_identifier (
    id          text NOT NULL,
    idtype      text NOT NULL,
	docid       text REFERENCES document(id),
    PRIMARY KEY (id, idtype)
);

CREATE TABLE project_other_identifier (
    id          text NOT NULL,
    idtype      text NOT NULL,
	projectid   text REFERENCES project(id),
    PRIMARY KEY (id, idtype)
);

CREATE TABLE journal_other_identifier (
    id          text NOT NULL,
    idtype      text NOT NULL,
	journalid   text REFERENCES journal(id),
    PRIMARY KEY (id, idtype)
);

CREATE TABLE organization_other_identifier (
	id           text NOT NULL,
    idtype       text NOT NULL,
	orgid        text REFERENCES organization(id),
    PRIMARY KEY (id, idtype)
);

-- RELATIONS BETWEEN ENTITIES --

CREATE TABLE doc_doc (
    docid1   text    NOT NULL REFERENCES document(id),
    docid2   text    NOT NULL REFERENCES document(id),
    reltype  text    NOT NULL,
    inferred  boolean default false,
    PRIMARY KEY (docid1, docid2, reltype)
);

CREATE TABLE doc_project (
    docid     text    NOT NULL REFERENCES document(id),
    projectid text    NOT NULL REFERENCES project(id),
    inferred  boolean default false,
    PRIMARY KEY (docid, projectid)
);

CREATE TABLE doc_journal (
    docid text NOT NULL REFERENCES document(id),
    journalid  text NOT NULL REFERENCES journal(id),
    inferred   boolean default false,
    PRIMARY KEY (docid, journalid)
);

CREATE TABLE project_organization (
	projectid              text NOT NULL REFERENCES project(id),
	orgid                  text NOT NULL REFERENCES organization(id),
	role                   text,
	activitytype           text,
	endofparticipation     text,
	eccontribution         text,
	contacttype            text,
	contacttitle           text,
	contactfirstnames      text,
	contactlastnames       text,
	contactfunction        text,
	contacttelephonenumber text,
	contactfaxnumber       text,
	contactform            text,
	inferred               boolean default false,
	contribution           numeric,
	currency               text,
	PRIMARY KEY (projectid,orgid)
);


CREATE TABLE doc_topic (
    docid    text NOT NULL REFERENCES document(id),
    topicid  integer NOT NULL,
    weight   numeric,
    experimentid text,
    inferred boolean default false,
	PRIMARY KEY (docid, topicid, experimentid),
	FOREIGN KEY (topicid, experimentid) REFERENCES topic(id, experimentid)
);

CREATE TABLE citation (
    docid       text    NOT NULL REFERENCES document(id),
    citedid     text    NOT NULL REFERENCES document(id),
    position    integer NOT NULL,
    citation    text    NOT NULL,
    inferred    boolean default false,
    PRIMARY KEY (docid, citedid)
);

-- RELATIONS BETWEEN ENTITIES (using alternative identifiers) --

CREATE TABLE doc_doc_other_id (
    docid1     text NOT NULL REFERENCES document(id),
    docid2     text NOT NULL,
    docid2type text NOT NULL,
    reltype    text NOT NULL,
    PRIMARY KEY (docid1, docid2, docid2type, reltype),
    FOREIGN KEY (docid2, docid2type) REFERENCES doc_other_identifier(id, idtype)
);

CREATE TABLE project_doc_other_id (
    projectid text NOT NULL REFERENCES project(id),
    docid     text NOT NULL,
    docidtype text NOT NULL,
    PRIMARY KEY (projectid, docid, docidtype),
    FOREIGN KEY (docid, docidtype) REFERENCES doc_other_identifier(id, idtype)
);

CREATE TABLE citation_other_identifier (
    docid       text    NOT NULL REFERENCES document(id),
    citedid     text    NOT NULL,
    citedidtype text    NOT NULL,
    position    integer NOT NULL,
    citation    text    NOT NULL,
    PRIMARY KEY (docid, citedid, citedidtype),
    FOREIGN KEY (citedid, citedidtype) REFERENCES doc_other_identifier(id, idtype)
);

-- ADDITIONAL TABLES --

CREATE TABLE doc_author (
    docid     text NOT NULL REFERENCES document(id),
    fullname  text NOT NULL,
    rank      integer,
    PRIMARY KEY (docid, fullname)
);

CREATE TABLE doc_fulltext (
    docid    text PRIMARY KEY REFERENCES document(id),
    fulltext text NOT NULL
);

CREATE TABLE doc_conference (
    docid       text PRIMARY KEY REFERENCES document(id),
    description text NOT NULL
);

CREATE TABLE doc_subject (
    docid    text NOT NULL REFERENCES document(id),
    subject  text NOT NULL,
    typology text NOT NULL,
    PRIMARY KEY (docid, subject, typology)
);

CREATE TABLE doc_pdbcode (
    docid   text NOT NULL REFERENCES document(id),
    pdbcode text NOT NULL,
    PRIMARY KEY (docid, pdbcode)
);

CREATE TABLE org_company_metrics (
	orgid                 text PRIMARY KEY REFERENCES organization(id),
	data_gathered         boolean,
    tangible_pre_market   int,
    tangible_market       int,
    intangible_pre_market int,
    intangible_market     int,
    innovation            boolean
);

CREATE TABLE org_company_innovation_texts (
	orgid              text REFERENCES organization(id),
	prediction_revised float,
	site_url           text,
	source             text,
	text_clean_gentle  text,
	text_clean_strong  text,
	text_is_duplicated boolean,
	PRIMARY KEY (orgid, site_url)
);

CREATE TABLE project_portfolio (
    projectid text PRIMARY KEY REFERENCES project(id),
    portfolio text NOT NULL,
    json json,
    administrative_data json,
    governance_data json,
    executive_summary json,
    final_report_summary json,
    impact json,
    objective json,
    title json
);


-- TABLES SUPPORTING TOPIC ANALYSIS
CREATE TABLE dbpediaresource
(
    id text COLLATE pg_catalog."default" NOT NULL,
    uri text COLLATE pg_catalog."default",
    abstract text COLLATE pg_catalog."default",
    wikiid text COLLATE pg_catalog."default",
    label text COLLATE pg_catalog."default",
    icd10 text,
    meshid text,
    mesh text,
    CONSTRAINT pk_dbpediaresource PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


CREATE TABLE public.dbpediaresourcetype
(
    typeuri text COLLATE pg_catalog."default" NOT NULL,
    resourceid text COLLATE pg_catalog."default" NOT NULL,
    typelabel text COLLATE pg_catalog."default",
    CONSTRAINT pk_dbpediaresourcetype PRIMARY KEY (typeuri, resourceid),
CONSTRAINT dbpediaresourcetype FOREIGN KEY (resourceid)
REFERENCES public.dbpediaresource (id) MATCH SIMPLE
  ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


CREATE TABLE dbpediaresourceacronym
(
    acronymuri text COLLATE pg_catalog."default",
    resourceid text COLLATE pg_catalog."default" NOT NULL,
    acronymlabel text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_dbpediaresourceacronym PRIMARY KEY (resourceid, acronymlabel, acronymuri)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dbpediaresourceacronym
    ADD CONSTRAINT dbpediaresourceacronym_resource_id_fkey FOREIGN KEY (resourceid)
    REFERENCES public.dbpediaresource (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX fki_dbpediaresourceacronym_resource_id_fkey
    ON public.dbpediaresourceacronym(resourceid);



CREATE TABLE dbpediaresourcecategory
(
    categoryuri text COLLATE pg_catalog."default" NOT NULL,
    resourceid text COLLATE pg_catalog."default" NOT NULL,
    categorylabel text COLLATE pg_catalog."default",
    CONSTRAINT pk_dbpediaresourcecategory PRIMARY KEY (categoryuri, resourceid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.dbpediaresourcecategory
    ADD CONSTRAINT dbpediaresourcecategory FOREIGN KEY (resourceid)
    REFERENCES public.dbpediaresource (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX fki_dbpediaresourcecategory
    ON public.dbpediaresourcecategory(resourceid);



CREATE TABLE doc_dbpediaresource
(
    docid text COLLATE pg_catalog."default" NOT NULL,
    resource text COLLATE pg_catalog."default" NOT NULL,
    support integer,
    count integer,
    similarity numeric,
    mention text COLLATE pg_catalog."default" NOT NULL,
    confidence integer,
    annotator text COLLATE pg_catalog."default",
    resourcecount integer,
    CONSTRAINT pk_pubdbpediaresource PRIMARY KEY (docid, resource, mention)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.doc_dbpediaresource
    ADD CONSTRAINT doc_dbpediaresource_docid_fkey FOREIGN KEY (docid)
    REFERENCES public.document (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX fki_doc_dbpediaresource_docid_fkey
    ON public.doc_dbpediaresource(docid);




CREATE TABLE topicanalysis
(
    topicid integer NOT NULL,
    itemtype smallint NOT NULL,
    item text COLLATE pg_catalog."default" NOT NULL,
    counts integer,
    batchid text COLLATE pg_catalog."default",
    experimentid text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "PK_TopicAnalysis" PRIMARY KEY (topicid, itemtype, item, experimentid),
    FOREIGN KEY (topicid, experimentid) REFERENCES topic(id, experimentid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



CREATE TABLE topicdetails
(
    topicid integer,
    itemtype smallint,
    weight numeric,
    totaltokens integer,
    batchid text COLLATE pg_catalog."default",
    experimentid text COLLATE pg_catalog."default",
    CONSTRAINT "PK_topicdetails" PRIMARY KEY (topicid, itemtype, experimentid),
    FOREIGN KEY (topicid, experimentid) REFERENCES topic(id, experimentid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



CREATE TABLE public.entityvector
(
    entityid text COLLATE pg_catalog."default",
    columnid integer,
    weight numeric,
    entitytype text COLLATE pg_catalog."default",
    experimentid text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



CREATE TABLE public.entitysimilarity
(
    entitytype text COLLATE pg_catalog."default",
    entityid1 text COLLATE pg_catalog."default",
    entityid2 text COLLATE pg_catalog."default",
    similarity numeric,
    experimentid text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



CREATE TABLE public.entitytopicdistribution
(
    batchid text COLLATE pg_catalog."default",
    topicid bigint,
    normweight numeric,
    experimentid text COLLATE pg_catalog."default",
    entityid text COLLATE pg_catalog."default",
    entitytype text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



CREATE TABLE public.experiment
(
    experimentid text COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default",
    metadata text COLLATE pg_catalog."default",
    initialsimilarity numeric,
    phraseboost integer,
    started timestamp with time zone,
    ended timestamp with time zone,
    status smallint,
    phase text COLLATE pg_catalog."default",
    lastupdate timestamp with time zone,
    CONSTRAINT pk_experiment PRIMARY KEY (experimentid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



CREATE TABLE public.expdiagnostics
(
    experimentid text COLLATE pg_catalog."default",
    batchid text COLLATE pg_catalog."default",
    entityid text COLLATE pg_catalog."default",
    entitytype smallint,
    scorename text COLLATE pg_catalog."default",
    score numeric,
	CONSTRAINT pk_expdiagnostics PRIMARY KEY (experimentid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



-- INDEXES --
CREATE INDEX citation_citedid_idx on citation(citedid);
CREATE INDEX citation_docid_idx on citation(docid);
CREATE INDEX citation_with_other_identifier_docid_idx on citation_other_identifier(docid);
CREATE INDEX doc_author_docid_idx on doc_author(docid);
CREATE INDEX doc_conference_docid_idx on doc_conference(docid);
CREATE INDEX doc_fulltext_docid_idx on doc_fulltext(docid);
CREATE INDEX doc_journal_docid_idx on doc_journal(docid);
CREATE INDEX doc_subject_docid_idx on doc_subject(docid);
CREATE INDEX doc_other_identifier_docid_idx on doc_other_identifier(docid);
CREATE INDEX doc_other_identifier_idtype_idx  ON doc_other_identifier(idtype);

CREATE INDEX doc_pdbcode_docid_idx on doc_pdbcode(docid);
CREATE INDEX doc_project_docid_idx on doc_project(docid);
CREATE INDEX doc_topic_docid_idx on doc_topic(docid);
CREATE INDEX doc_doc_docid1_idx on doc_doc(docid1);
CREATE INDEX doc_doc_docid2_idx on doc_doc(docid2);
CREATE INDEX document_doctype_idx on document(doctype);
CREATE INDEX idx_10_dbpediaresourceacronym ON dbpediaresourceacronym USING btree (resourceid COLLATE pg_catalog."default", acronymlabel COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_11_dbpediaresourceacronym ON dbpediaresourceacronym USING btree (acronymlabel COLLATE pg_catalog."default", resourceid COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_12_dbpediaresourcecategory ON dbpediaresourcecategory USING btree (resourceid COLLATE pg_catalog."default", categoryuri COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_13_dbpediaresourcecategory ON dbpediaresourcecategory USING btree (categoryuri COLLATE pg_catalog."default", resourceid COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_pubdbpedia_resource ON doc_dbpediaresource USING btree (resource COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_pubid_resource_count_pubdbpediaresource ON doc_dbpediaresource USING btree (docid COLLATE pg_catalog."default", resource COLLATE pg_catalog."default", count) TABLESPACE pg_default;
CREATE INDEX idx_41_pubtopic ON doc_topic USING btree (topicid, weight, docid COLLATE pg_catalog."default", experimentid COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_42_pubtopic ON doc_topic USING btree (docid COLLATE pg_catalog."default", experimentid COLLATE pg_catalog."default", weight, topicid) TABLESPACE pg_default;
CREATE INDEX idx_46_topicdescription ON topic USING btree (experimentid COLLATE pg_catalog."default", id) TABLESPACE pg_default;
CREATE INDEX idx_47_topicdescription ON topic USING btree (experimentid COLLATE pg_catalog."default", visibilityindex, title COLLATE pg_catalog."default", id) TABLESPACE pg_default;
CREATE INDEX idx_48_topicdescription ON topic USING btree (id, experimentid COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX "idx_topicAnalysis_exp_itemtype" ON topicanalysis USING btree (experimentid COLLATE pg_catalog."default", itemtype) TABLESPACE pg_default;
CREATE INDEX "idx_topicAnalysis_exp_topicId" ON topicanalysis USING btree (experimentid COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX "EntitySimilarity_Similarity" ON entitysimilarity USING btree (similarity) TABLESPACE pg_default;
CREATE INDEX idx_14_entitysimilarity ON entitysimilarity USING btree (experimentid COLLATE pg_catalog."default", similarity, entitytype COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_15_entitysimilarity ON entitysimilarity USING btree (entitytype COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX "IX_EntityType_ExperimentId" ON entitytopicdistribution USING btree (entitytype COLLATE pg_catalog."default", experimentid COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_19_experiment ON experiment USING btree (experimentid COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX fki_dbpediaresourcetype ON public.dbpediaresourcetype USING btree (resourceid COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_12_dbpediaresourcetype ON public.dbpediaresourcetype USING btree (resourceid COLLATE pg_catalog."default", typeuri COLLATE pg_catalog."default") TABLESPACE pg_default;
CREATE INDEX idx_13_dbpediaresourcetype ON public.dbpediaresourcetype USING btree (typeuri COLLATE pg_catalog."default", resourceid COLLATE pg_catalog."default") TABLESPACE pg_default;

-- VIEWS --


CREATE VIEW guideline_ids AS (
  SELECT d.id,
         json_agg(json_build_object(di.idtype, di.id))::jsonb AS ids,
         d.doctype AS type,
         d.title,
         d.pubyear,
         d.repository AS originator,
         d.collection AS providercollection,
         d.abstract
  FROM document d
    JOIN doc_other_identifier di ON d.id = di.docid
  WHERE d.doctype = 'guideline'::text
  GROUP BY d.id
)

CREATE VIEW guideline_localids AS (
  SELECT DISTINCT ON ((t1.doc -> 'guidelineLocalID'::text)) t1.doc ->> 'guidelineLocalID'::text AS localid,
  g.id,
  g.type,
  g.title,
  g.pubyear,
  g.originator,
  g.providercollection,
  g.abstract,
  g.ids
  FROM ( SELECT * FROM guideline_ids) g,
  LATERAL jsonb_array_elements(g.ids) WITH ORDINALITY t1(doc, rn)
  WHERE t1.doc ->> 'guidelineLocalID'::text IS NOT NULL
  ORDER BY (t1.doc -> 'guidelineLocalID'::text), t1.rn
)
CREATE VIEW guidelines AS (
  SELECT t.id,
         t.localid,
         t.type,
         t.title,
         t.pubyear,
         t.originator,
         t.providercollection,
         t.abstract,
         t.ids,
         json_agg(json_build_object(ddo_m.docid2type, ddo_m.docid2))::jsonb AS matchedreferences,
         json_agg(json_build_object(ddo_a.docid2type, ddo_a.docid2))::jsonb AS allreferences
 FROM (SELECT * FROM guideline_localids) t
  JOIN doc_doc_other_id ddo_m ON t.id = ddo_m.docid1
  JOIN doc_doc_other_id ddo_a ON t.id = ddo_a.docid1
  WHERE ddo_m.reltype = 'guidelines_matched' and ddo_a.reltype = 'guidelines_all'
  GROUP BY t.id, t.localid, t.type, t.title, t.pubyear, t.originator, t.providercollection, t.abstract, t.ids
)


CREATE MATERIALIZED VIEW organization_view AS SELECT 
	o.*,
	(c.orgid IS NOT NULL) AS company,
	c.data_gathered,
	c.tangible_pre_market,
	c.tangible_market,
	c.intangible_pre_market,
	c.intangible_market,
	c.innovation,
	array_to_json(array_agg(json_build_object('id',oi.id,'type',oi.idtype))) AS other_ids,
	array_to_json(array_agg(json_build_object('id',p.id,'name',p.title,'role',po.role))) AS projects
FROM 
	organization o 
	LEFT OUTER JOIN org_company_metrics c ON (o.id = c.orgid)
	LEFT OUTER JOIN organization_other_identifier oi ON (o.id = oi.orgid)
	LEFT OUTER JOIN project_organization po ON (o.id = po.orgid)
	LEFT OUTER JOIN project p ON (p.id = po.projectid)
GROUP BY o.id, c.orgid;
	   
CREATE MATERIALIZED VIEW project_view AS SELECT
	p.*,
	array_to_json(array_agg(json_build_object('id',oi.id,'type',oi.idtype))) AS other_ids,
	array_to_json(array_agg(json_build_object('id',o.id,'name',o.name,'role',po.role))) AS participants
FROM 
	project p 
	LEFT OUTER JOIN project_other_identifier oi ON (p.id = oi.projectid)
	LEFT OUTER JOIN project_organization po ON (p.id = po.projectid)
	LEFT OUTER JOIN organization o ON (o.id = po.orgid)
GROUP BY p.id;

CREATE MATERIALIZED VIEW ec_publications_by_program AS SELECT
	p.fundinglevel0,
	p.fundinglevel1,
	count(dp.docid)
FROM
	project p
	LEFT OUTER JOIN doc_project dp ON (p.id = dp.projectid)
	LEFT OUTER JOIN document d ON (d.id = dp.docid)
WHERE d.doctype = 'publication' AND p.funder = 'EC'
GROUP BY p.fundinglevel0, p.fundinglevel1
ORDER BY count DESC;


CREATE MATERIALIZED VIEW ec_patents_by_program AS SELECT
	p.fundinglevel0,
	p.fundinglevel1,
	count(dp.docid)
FROM
	project p
	LEFT OUTER JOIN doc_project dp ON (p.id = dp.projectid)
	LEFT OUTER JOIN document d ON (d.id = dp.docid)
WHERE d.doctype = 'patent' AND p.funder = 'EC'
GROUP BY p.fundinglevel0, p.fundinglevel1
ORDER BY count DESC;

-- EC projects do not have any reference to guidelines
CREATE MATERIALIZED VIEW guidelines_by_program AS
SELECT
 	p.funder,
	p.fundinglevel0,
	p.fundinglevel1,
	count(dp.docid)
FROM
	project p
	LEFT OUTER JOIN doc_project dp ON (p.id = dp.projectid)
	LEFT OUTER JOIN document d ON (d.id = dp.docid)
WHERE d.doctype = 'guideline'
GROUP BY p.fundinglevel0, p.fundinglevel1, p.funder
ORDER BY count DESC;


CREATE MATERIALIZED VIEW ec_publications_by_year AS 
SELECT
	substr(d.pubyear, 1, 4)::integer AS year,
	count(*)
FROM
	document d
	LEFT OUTER JOIN doc_project dp ON (d.id = dp.docid)
	LEFT OUTER JOIN project p ON (p.id = dp.projectid)
WHERE d.doctype = 'publication' AND p.funder = 'EC' AND d.pubyear ~ '^\d{4}(\-\d{2}\-\d{2})?$' AND substr(d.pubyear, 1, 4)::integer >= 2007
GROUP BY year
ORDER BY year DESC;


CREATE MATERIALIZED VIEW ec_patents_by_year AS
SELECT
	d.pubyear::integer AS year,
	count(*)
FROM
	document d
	LEFT OUTER JOIN doc_project dp ON (d.id = dp.docid)
	LEFT OUTER JOIN project p ON (p.id = dp.projectid)
WHERE d.doctype = 'patent' AND p.funder = 'EC' AND d.pubyear ~ '^\d{4}$' AND d.pubyear::integer >= 2007
GROUP BY year
ORDER BY year DESC;


CREATE MATERIALIZED VIEW ec_funding_by_program AS
	SELECT p.fundinglevel0 AS funding,
	p.fundinglevel1 AS program,
	sum(total_cost) AS total_cost,
	sum(contribution) AS contribution
FROM project p
WHERE p.funder = 'EC' AND fundinglevel1 IS NOT NULL AND total_cost IS NOT NULL
GROUP BY p.fundinglevel0, p.fundinglevel1;

CREATE MATERIALIZED VIEW ec_funding_by_year AS
    SELECT EXTRACT(YEAR FROM startdate::date) AS year,
    sum(total_cost) as total_cost,
    sum(contribution) as contribution
FROM
    project p
    JOIN project_portfolio pp ON (p.id = pp.projectid)
WHERE funder = 'EC'
GROUP BY year
ORDER BY year DESC ;



CREATE OR REPLACE VIEW public.doctxt_view AS
 SELECT document.id AS docid,
    substr((((COALESCE(document.title, ''::text) || ' '::text) || COALESCE(document.abstract, ''::text)) || ' '::text) || substr(COALESCE(doc_fulltext.fulltext, ''::text), 300, 7000), 0, 10000) AS text,
    document.title,
    COALESCE(document.abstract, ''::text) AS abstract,
    substr(COALESCE(doc_fulltext.fulltext, ''::text), 300, 7000) AS fulltext,
    document.batchid,
    document.doctype
   FROM document
     LEFT JOIN doc_fulltext ON doc_fulltext.docid = document.id
  WHERE NOT (COALESCE(doc_fulltext.fulltext, ''::text) || COALESCE(document.abstract, ''::text)) = ''::text;


CREATE OR REPLACE VIEW public.doc_funder_view AS
 SELECT doc_project.docid,
    project.funder
   FROM doc_project
     JOIN project ON project.id = doc_project.projectid;


CREATE OR REPLACE VIEW public.docsideinfo_view AS
 SELECT document.docid,
    string_agg(DISTINCT
        CASE
            WHEN doc_subject.typology = 'keyword'::text THEN doc_subject.subject
            ELSE ''::text
        END, ','::text) AS keywords,
    string_agg(DISTINCT
        CASE
            WHEN doc_subject.typology = 'mesheuropmc'::text THEN doc_subject.subject
            ELSE ''::text
        END, ','::text) AS meshterms,
    string_agg(DISTINCT doc_dbpediares.resource, ','::text) AS dbpediaresources,
    document.batchid
   FROM doctxt_view document
     LEFT JOIN doc_subject ON doc_subject.docid = document.docid
     LEFT JOIN ( SELECT (doc_dbpediaresource.resource || ';'::text) || doc_dbpediaresource.resourcecount AS resource,
            doc_dbpediaresource.docid
           FROM doc_dbpediaresource
          WHERE doc_dbpediaresource.support < 1500 AND length(doc_dbpediaresource.mention) > 3 AND doc_dbpediaresource.similarity > 0.9 AND doc_dbpediaresource.resourcecount > 2) doc_dbpediares ON doc_dbpediares.docid = document.docid
  GROUP BY document.docid, document.batchid;

CREATE OR REPLACE VIEW public.topic_view AS
 SELECT topicanalysis.topicid,
    topicanalysis.item,
    topicanalysis.itemtype,
    freqitems.topicscnt,
    topicanalysis.counts *
        CASE topicanalysis.itemtype
            WHEN '-1'::integer THEN experiment.phraseboost::bigint
            ELSE 1::bigint
        END AS counts,
        CASE topicanalysis.itemtype
            WHEN '-1'::integer THEN experiment.phraseboost::bigint
            ELSE 1::bigint
        END AS typeweight,
    freqitems.powsum / (freqitems.totalsum * freqitems.totalsum) AS discrweight,
    round(
        CASE topicanalysis.itemtype
            WHEN '-1'::integer THEN experiment.phraseboost::bigint
            ELSE 1::bigint
        END::double precision * freqitems.powsum / (freqitems.totalsum * freqitems.totalsum) * topicanalysis.counts::double precision) AS weightedcounts,
    topicanalysis.experimentid
   FROM topicanalysis
     JOIN experiment ON topicanalysis.experimentid = experiment.experimentid
     JOIN ( SELECT topicanalysis_1.experimentid,
            topicanalysis_1.item,
            topicanalysis_1.itemtype,
            count(*) AS topicscnt,
            sum(topicanalysis_1.counts::real * topicanalysis_1.counts::double precision) AS powsum,
            sum(topicanalysis_1.counts)::real AS totalsum
           FROM topicanalysis topicanalysis_1
          WHERE (topicanalysis_1.itemtype = '-1'::integer OR topicanalysis_1.itemtype = 0) AND topicanalysis_1.counts > 3
          GROUP BY topicanalysis_1.experimentid, topicanalysis_1.itemtype, topicanalysis_1.item) freqitems ON freqitems.experimentid = topicanalysis.experimentid AND freqitems.itemtype = topicanalysis.itemtype AND freqitems.item = topicanalysis.item
  WHERE (topicanalysis.itemtype = '-1'::integer OR topicanalysis.itemtype = 0) AND topicanalysis.counts > 3
  ORDER BY topicanalysis.experimentid, topicanalysis.topicid, (topicanalysis.counts *
        CASE topicanalysis.itemtype
            WHEN '-1'::integer THEN experiment.phraseboost::bigint
            ELSE 1::bigint
        END) DESC;

CREATE OR REPLACE VIEW public.trends_view AS
 SELECT newtopics.topicid,
    newtopics.title,
    round(newtopics.topicavg, 5) AS newavgweight,
    round(oldtopics.topicavg, 5) AS oldavgweight,
    round(newtopics.topicavg / oldtopics.topicavg, 2) AS trendindex,
    newtopics.experimentid
   FROM ( SELECT topicdiffs.topicid,
            avg(abs(topicdiffs.topicdiff)) / topicdiffs.topicavg AS var,
            topicdiffs.topicavg,
            topicdiffs.experimentid,
            topic.title,
            topicdiffs.entitytype
           FROM ( SELECT entitytopicdistribution.topicid,
                    entitytopicdistribution.batchid,
                    entitytopicdistribution.normweight - topicavgs.topicavg AS topicdiff,
                    topicavgs.topicavg,
                    entitytopicdistribution.experimentid,
                    entitytopicdistribution.entitytype
                   FROM entitytopicdistribution
                     JOIN ( SELECT entitytopicdistribution_1.topicid,
                            avg(entitytopicdistribution_1.normweight) AS topicavg,
                            entitytopicdistribution_1.experimentid,
                            entitytopicdistribution_1.entitytype
                           FROM entitytopicdistribution entitytopicdistribution_1
                          WHERE (entitytopicdistribution_1.batchid ~~ '19%'::text OR (entitytopicdistribution_1.batchid = ANY (ARRAY['2000'::text, '2001'::text, '2002'::text, '2003'::text, '2004'::text, '2005'::text, '2006'::text, '2007'::text, '2008'::text, '2009'::text, '2010'::text, '2011'::text]))) AND entitytopicdistribution_1.entitytype = 'CorpusTrend'::text
                          GROUP BY entitytopicdistribution_1.topicid, entitytopicdistribution_1.experimentid, entitytopicdistribution_1.entitytype) topicavgs ON entitytopicdistribution.topicid = topicavgs.topicid AND entitytopicdistribution.entitytype = topicavgs.entitytype AND entitytopicdistribution.experimentid = topicavgs.experimentid) topicdiffs
             JOIN topic ON topic.id = topicdiffs.topicid AND topic.experimentid = topicdiffs.experimentid AND topic.visibilityindex > 0
          GROUP BY topicdiffs.topicid, topicdiffs.topicavg, topicdiffs.experimentid, topicdiffs.entitytype, topic.title) oldtopics
     JOIN ( SELECT topicdiffs.topicid,
            avg(abs(topicdiffs.topicdiff)) / topicdiffs.topicavg AS var,
            topicdiffs.topicavg,
            topicdiffs.experimentid,
            topic.title,
            topicdiffs.entitytype
           FROM ( SELECT entitytopicdistribution.topicid,
                    entitytopicdistribution.batchid,
                    entitytopicdistribution.normweight - topicavgs.topicavg AS topicdiff,
                    topicavgs.topicavg,
                    entitytopicdistribution.experimentid,
                    entitytopicdistribution.entitytype
                   FROM entitytopicdistribution
                     JOIN ( SELECT entitytopicdistribution_1.topicid,
                            avg(entitytopicdistribution_1.normweight) AS topicavg,
                            entitytopicdistribution_1.experimentid,
                            entitytopicdistribution_1.entitytype
                           FROM entitytopicdistribution entitytopicdistribution_1
                          WHERE (entitytopicdistribution_1.batchid = ANY (ARRAY['2012'::text, '2013'::text, '2014'::text, '2015'::text, '2016'::text, '2017'::text, '2018'::text])) AND entitytopicdistribution_1.entitytype = 'CorpusTrend'::text
                          GROUP BY entitytopicdistribution_1.topicid, entitytopicdistribution_1.experimentid, entitytopicdistribution_1.entitytype) topicavgs ON entitytopicdistribution.topicid = topicavgs.topicid AND entitytopicdistribution.entitytype = topicavgs.entitytype AND entitytopicdistribution.experimentid = topicavgs.experimentid) topicdiffs
             JOIN topic ON topic.id = topicdiffs.topicid AND topic.experimentid = topicdiffs.experimentid AND topic.visibilityindex > 0
          GROUP BY topicdiffs.topicid, topicdiffs.topicavg, topicdiffs.experimentid, topicdiffs.entitytype, topic.title) newtopics ON oldtopics.topicid = newtopics.topicid AND oldtopics.experimentid = newtopics.experimentid AND oldtopics.entitytype = newtopics.entitytype
  ORDER BY (round(newtopics.topicavg / oldtopics.topicavg, 2)) DESC;